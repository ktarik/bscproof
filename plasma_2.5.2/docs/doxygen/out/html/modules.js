var modules =
[
    [ "Simple Interface - Single Real", "group__float.html", "group__float" ],
    [ "Simple Interface - Double Real", "group__double.html", "group__double" ],
    [ "Simple Interface - Single Complex", "group__PLASMA__Complex32__t.html", "group__PLASMA__Complex32__t" ],
    [ "Simple Interface - Double Complex", "group__PLASMA__Complex64__t.html", "group__PLASMA__Complex64__t" ],
    [ "Advanced Interface: Synchronous - Single Real", "group__float__Tile.html", "group__float__Tile" ],
    [ "Advanced Interface: Synchronous - Double Real", "group__double__Tile.html", "group__double__Tile" ],
    [ "Advanced Interface: Synchronous - Single Complex", "group__PLASMA__Complex32__t__Tile.html", "group__PLASMA__Complex32__t__Tile" ],
    [ "Advanced Interface: Synchronous - Double Complex", "group__PLASMA__Complex64__t__Tile.html", "group__PLASMA__Complex64__t__Tile" ],
    [ "Advanced Interface: Asynchronous - Single Real", "group__float__Tile__Async.html", "group__float__Tile__Async" ],
    [ "Advanced Interface: Asynchronous - Double Real", "group__double__Tile__Async.html", "group__double__Tile__Async" ],
    [ "Advanced Interface: Asynchronous - Single Complex", "group__PLASMA__Complex32__t__Tile__Async.html", "group__PLASMA__Complex32__t__Tile__Async" ],
    [ "Advanced Interface: Asynchronous - Double Complex", "group__PLASMA__Complex64__t__Tile__Async.html", "group__PLASMA__Complex64__t__Tile__Async" ]
];