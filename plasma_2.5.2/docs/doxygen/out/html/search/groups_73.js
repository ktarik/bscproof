var searchData=
[
  ['simple_20interface_20_2d_20double_20real',['Simple Interface - Double Real',['../group__double.html',1,'']]],
  ['simple_20interface_20_2d_20single_20real',['Simple Interface - Single Real',['../group__float.html',1,'']]],
  ['simple_20interface_20_2d_20single_20complex',['Simple Interface - Single Complex',['../group__PLASMA__Complex32__t.html',1,'']]],
  ['simple_20interface_20_2d_20double_20complex',['Simple Interface - Double Complex',['../group__PLASMA__Complex64__t.html',1,'']]]
];
