var searchData=
[
  ['simple_20interface_20_2d_20double_20real',['Simple Interface - Double Real',['../group__double.html',1,'']]],
  ['simple_20interface_20_2d_20single_20real',['Simple Interface - Single Real',['../group__float.html',1,'']]],
  ['simple_20interface_20_2d_20single_20complex',['Simple Interface - Single Complex',['../group__PLASMA__Complex32__t.html',1,'']]],
  ['simple_20interface_20_2d_20double_20complex',['Simple Interface - Double Complex',['../group__PLASMA__Complex64__t.html',1,'']]],
  ['status',['status',['../structplasma__request__t_a86cd12f11d72a3aa4fb94f6cc406c507.html#a86cd12f11d72a3aa4fb94f6cc406c507',1,'plasma_request_t::status()'],['../structplasma__sequence__t_adf37ddea62dfaaa498c0defe68be01a4.html#adf37ddea62dfaaa498c0defe68be01a4',1,'plasma_sequence_t::status()']]]
];
