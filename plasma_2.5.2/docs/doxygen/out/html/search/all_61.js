var searchData=
[
  ['a12',['A12',['../structplasma__desc__t_a5081e56a01179e8b9305912676909a13.html#a5081e56a01179e8b9305912676909a13',1,'plasma_desc_t']]],
  ['a21',['A21',['../structplasma__desc__t_a9749348dffa9c3584312c581f4c2bc20.html#a9749348dffa9c3584312c581f4c2bc20',1,'plasma_desc_t']]],
  ['a22',['A22',['../structplasma__desc__t_ad8485dcb9895aee64a447c70642cb554.html#ad8485dcb9895aee64a447c70642cb554',1,'plasma_desc_t']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20double_20real',['Advanced Interface: Synchronous - Double Real',['../group__double__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20double_20real',['Advanced Interface: Asynchronous - Double Real',['../group__double__Tile__Async.html',1,'']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20single_20real',['Advanced Interface: Synchronous - Single Real',['../group__float__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20single_20real',['Advanced Interface: Asynchronous - Single Real',['../group__float__Tile__Async.html',1,'']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20single_20complex',['Advanced Interface: Synchronous - Single Complex',['../group__PLASMA__Complex32__t__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20single_20complex',['Advanced Interface: Asynchronous - Single Complex',['../group__PLASMA__Complex32__t__Tile__Async.html',1,'']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20double_20complex',['Advanced Interface: Synchronous - Double Complex',['../group__PLASMA__Complex64__t__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20double_20complex',['Advanced Interface: Asynchronous - Double Complex',['../group__PLASMA__Complex64__t__Tile__Async.html',1,'']]]
];
