var searchData=
[
  ['advanced_20interface_3a_20synchronous_20_2d_20double_20real',['Advanced Interface: Synchronous - Double Real',['../group__double__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20double_20real',['Advanced Interface: Asynchronous - Double Real',['../group__double__Tile__Async.html',1,'']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20single_20real',['Advanced Interface: Synchronous - Single Real',['../group__float__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20single_20real',['Advanced Interface: Asynchronous - Single Real',['../group__float__Tile__Async.html',1,'']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20single_20complex',['Advanced Interface: Synchronous - Single Complex',['../group__PLASMA__Complex32__t__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20single_20complex',['Advanced Interface: Asynchronous - Single Complex',['../group__PLASMA__Complex32__t__Tile__Async.html',1,'']]],
  ['advanced_20interface_3a_20synchronous_20_2d_20double_20complex',['Advanced Interface: Synchronous - Double Complex',['../group__PLASMA__Complex64__t__Tile.html',1,'']]],
  ['advanced_20interface_3a_20asynchronous_20_2d_20double_20complex',['Advanced Interface: Asynchronous - Double Complex',['../group__PLASMA__Complex64__t__Tile__Async.html',1,'']]]
];
