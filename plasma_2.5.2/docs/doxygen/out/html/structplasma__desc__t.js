var structplasma__desc__t =
[
    [ "A12", "structplasma__desc__t_a5081e56a01179e8b9305912676909a13.html#a5081e56a01179e8b9305912676909a13", null ],
    [ "A21", "structplasma__desc__t_a9749348dffa9c3584312c581f4c2bc20.html#a9749348dffa9c3584312c581f4c2bc20", null ],
    [ "A22", "structplasma__desc__t_ad8485dcb9895aee64a447c70642cb554.html#ad8485dcb9895aee64a447c70642cb554", null ],
    [ "bsiz", "structplasma__desc__t_a9611b974738f4accbb79e366fdcba787.html#a9611b974738f4accbb79e366fdcba787", null ],
    [ "dtyp", "structplasma__desc__t_ae3456ab5010805f78b76debb9b3ec2b3.html#ae3456ab5010805f78b76debb9b3ec2b3", null ],
    [ "i", "structplasma__desc__t_a7508194cedb3083d9152c4e2f957dc88.html#a7508194cedb3083d9152c4e2f957dc88", null ],
    [ "j", "structplasma__desc__t_a1d5e8aa30ce545ac2d9c2e75e201e976.html#a1d5e8aa30ce545ac2d9c2e75e201e976", null ],
    [ "lm", "structplasma__desc__t_a20df9ed35d79a68537e991794ed16c1f.html#a20df9ed35d79a68537e991794ed16c1f", null ],
    [ "lm1", "structplasma__desc__t_abcef9d7944f3e6a79a1140389a5c887d.html#abcef9d7944f3e6a79a1140389a5c887d", null ],
    [ "lmt", "structplasma__desc__t_a6c43b11d26b88311906eab54db5d1ad2.html#a6c43b11d26b88311906eab54db5d1ad2", null ],
    [ "ln", "structplasma__desc__t_a0deb48b03bc6427bdd3770aeea1d5f36.html#a0deb48b03bc6427bdd3770aeea1d5f36", null ],
    [ "ln1", "structplasma__desc__t_a4e8774165bf4ec4a438b46148bf18c7e.html#a4e8774165bf4ec4a438b46148bf18c7e", null ],
    [ "lnt", "structplasma__desc__t_a47634188eab194b20f78b8c2c32e9582.html#a47634188eab194b20f78b8c2c32e9582", null ],
    [ "m", "structplasma__desc__t_a7f9b40af027da0a8f9af989794f31a8a.html#a7f9b40af027da0a8f9af989794f31a8a", null ],
    [ "mat", "structplasma__desc__t_abcec2c322f8d2461e8afa6c0e7f52220.html#abcec2c322f8d2461e8afa6c0e7f52220", null ],
    [ "mb", "structplasma__desc__t_a8512ed221aec5b7db0e31d02acc5b5c7.html#a8512ed221aec5b7db0e31d02acc5b5c7", null ],
    [ "mt", "structplasma__desc__t_a35d0d7ad5f5927bb551a357a174152c2.html#a35d0d7ad5f5927bb551a357a174152c2", null ],
    [ "n", "structplasma__desc__t_a1deb8d67b1fca78449a40ba7a056cafd.html#a1deb8d67b1fca78449a40ba7a056cafd", null ],
    [ "nb", "structplasma__desc__t_af3770d8c57330e1e22a149f46cddb272.html#af3770d8c57330e1e22a149f46cddb272", null ],
    [ "nt", "structplasma__desc__t_a7595a9db8b1b8360b5fa9726b998a117.html#a7595a9db8b1b8360b5fa9726b998a117", null ]
];