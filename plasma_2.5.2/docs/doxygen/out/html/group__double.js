var group__double =
[
    [ "PLASMA_dgebrd", "group__double_gaabe52f420c4ae6d1db03f9fb37b8045f.html#gaabe52f420c4ae6d1db03f9fb37b8045f", null ],
    [ "PLASMA_dgecon", "group__double_gaffb6839f6bec9ca91ddba211efecb14c.html#gaffb6839f6bec9ca91ddba211efecb14c", null ],
    [ "PLASMA_dgelqf", "group__double_ga60fffe07485af5c38c0447efccf8ccfe.html#ga60fffe07485af5c38c0447efccf8ccfe", null ],
    [ "PLASMA_dgelqs", "group__double_ga092545f106c9fae67b82eb65f11ca4ab.html#ga092545f106c9fae67b82eb65f11ca4ab", null ],
    [ "PLASMA_dgels", "group__double_ga4bfc0cdaf567c26828f484e54d457d92.html#ga4bfc0cdaf567c26828f484e54d457d92", null ],
    [ "PLASMA_dgemm", "group__double_ga5c560bea8139857be91339582ba830ef.html#ga5c560bea8139857be91339582ba830ef", null ],
    [ "PLASMA_dgeqp3", "group__double_ga9d11b150328fa64daa7c9f798baf3ea1.html#ga9d11b150328fa64daa7c9f798baf3ea1", null ],
    [ "PLASMA_dgeqrf", "group__double_gaad8136b3520b4bda7261d3f921c8a740.html#gaad8136b3520b4bda7261d3f921c8a740", null ],
    [ "PLASMA_dgeqrs", "group__double_gaa133b69ef592ef01466fd93b6ac7689d.html#gaa133b69ef592ef01466fd93b6ac7689d", null ],
    [ "PLASMA_dgesdd", "group__double_ga90d8245b7c9b7124c35c485f0e4f63b7.html#ga90d8245b7c9b7124c35c485f0e4f63b7", null ],
    [ "PLASMA_dgesv", "group__double_gae380e95b53e07b9b1261aec244191c0f.html#gae380e95b53e07b9b1261aec244191c0f", null ],
    [ "PLASMA_dgesv_incpiv", "group__double_gaaf143240840f5315cf672d66dead1111.html#gaaf143240840f5315cf672d66dead1111", null ],
    [ "PLASMA_dgesvd", "group__double_ga02d390fe38b9b983a604f2a8557abafa.html#ga02d390fe38b9b983a604f2a8557abafa", null ],
    [ "PLASMA_dgetrf", "group__double_gaa626e37ec710bfb5c98fef7b00511ea8.html#gaa626e37ec710bfb5c98fef7b00511ea8", null ],
    [ "PLASMA_dgetrf_incpiv", "group__double_gad1a9ffdf2bf95696d94eac58177c3964.html#gad1a9ffdf2bf95696d94eac58177c3964", null ],
    [ "PLASMA_dgetrf_nopiv", "group__double_ga8a7ad8ad13ca055f1cece944ffa8185a.html#ga8a7ad8ad13ca055f1cece944ffa8185a", null ],
    [ "PLASMA_dgetrf_tntpiv", "group__double_gac5e965a28bb5189491690e9cdfcc2b61.html#gac5e965a28bb5189491690e9cdfcc2b61", null ],
    [ "PLASMA_dgetri", "group__double_gaef9247e762016885c0b31dc28356bab7.html#gaef9247e762016885c0b31dc28356bab7", null ],
    [ "PLASMA_dgetrs", "group__double_gaff3c22db4affbede58e22311320a2b85.html#gaff3c22db4affbede58e22311320a2b85", null ],
    [ "PLASMA_dgetrs_incpiv", "group__double_ga39d27f0353921e5b63e0fdc9cca4b19c.html#ga39d27f0353921e5b63e0fdc9cca4b19c", null ],
    [ "PLASMA_dlacpy", "group__double_gaa1c69f5b82fa24bd925055b4e0595dab.html#gaa1c69f5b82fa24bd925055b4e0595dab", null ],
    [ "PLASMA_dlange", "group__double_ga51325eef4ca9e4ccd314358ec47535ec.html#ga51325eef4ca9e4ccd314358ec47535ec", null ],
    [ "PLASMA_dlansy", "group__double_ga7d2422fb6092b7ad92fa0121d5c2d792.html#ga7d2422fb6092b7ad92fa0121d5c2d792", null ],
    [ "PLASMA_dLapack_to_Tile", "group__double_ga36e9254b8f24778e1aed6bcee1165450.html#ga36e9254b8f24778e1aed6bcee1165450", null ],
    [ "PLASMA_dlaset", "group__double_ga2b2e191b49f6e0fede1bcd352bcd14af.html#ga2b2e191b49f6e0fede1bcd352bcd14af", null ],
    [ "PLASMA_dlaswp", "group__double_ga2f62c99800ab0165fa0fb2c58cb2869a.html#ga2f62c99800ab0165fa0fb2c58cb2869a", null ],
    [ "PLASMA_dlaswpc", "group__double_gaf05cd57bcd5a74823cc4f71d91a299bc.html#gaf05cd57bcd5a74823cc4f71d91a299bc", null ],
    [ "PLASMA_dlauum", "group__double_ga67ff8abfce111a501fb5cec37333756f.html#ga67ff8abfce111a501fb5cec37333756f", null ],
    [ "PLASMA_dorglq", "group__double_gae71a8dbbe843b68aca052074db93409d.html#gae71a8dbbe843b68aca052074db93409d", null ],
    [ "PLASMA_dorgqr", "group__double_ga5137c6e07ebf2faddae220daa28999ba.html#ga5137c6e07ebf2faddae220daa28999ba", null ],
    [ "PLASMA_dormlq", "group__double_ga7c70d5b53e69f9ab564126c78938002a.html#ga7c70d5b53e69f9ab564126c78938002a", null ],
    [ "PLASMA_dormqr", "group__double_ga9623493d89eb656c0f5aa44c7772294c.html#ga9623493d89eb656c0f5aa44c7772294c", null ],
    [ "PLASMA_dplgsy", "group__double_ga267c9f85256181c2024279e1dc55e84b.html#ga267c9f85256181c2024279e1dc55e84b", null ],
    [ "PLASMA_dplrnt", "group__double_gafca5b7ac3183e0a107df9f88ecde4cdf.html#gafca5b7ac3183e0a107df9f88ecde4cdf", null ],
    [ "PLASMA_dpocon", "group__double_ga2e52f461973867ecb409cfaa3d4cac36.html#ga2e52f461973867ecb409cfaa3d4cac36", null ],
    [ "PLASMA_dposv", "group__double_ga83015e9476d13f0c9f056eac9b54804f.html#ga83015e9476d13f0c9f056eac9b54804f", null ],
    [ "PLASMA_dpotrf", "group__double_ga08c3851e861086761123a375b8472df2.html#ga08c3851e861086761123a375b8472df2", null ],
    [ "PLASMA_dpotri", "group__double_ga01b70786730b5e4c314b4584fe3697c5.html#ga01b70786730b5e4c314b4584fe3697c5", null ],
    [ "PLASMA_dpotrs", "group__double_ga2d3c0509f11f1e9a9cf06b95109e62f5.html#ga2d3c0509f11f1e9a9cf06b95109e62f5", null ],
    [ "PLASMA_dsgesv", "group__double_ga50fe4c984e8994fe22aac09c0f6c3422.html#ga50fe4c984e8994fe22aac09c0f6c3422", null ],
    [ "PLASMA_dsposv", "group__double_gaf4c055accc54907abd1ea3a85555c9b4.html#gaf4c055accc54907abd1ea3a85555c9b4", null ],
    [ "PLASMA_dsungesv", "group__double_ga5ad1a5e52e906d9add120b37fc4301fc.html#ga5ad1a5e52e906d9add120b37fc4301fc", null ],
    [ "PLASMA_dsyev", "group__double_gac7ea19b1441c1325f45c0f6a9cfd8a8a.html#gac7ea19b1441c1325f45c0f6a9cfd8a8a", null ],
    [ "PLASMA_dsyevd", "group__double_ga63eb5c9b94563a57c49a86e8a6c93616.html#ga63eb5c9b94563a57c49a86e8a6c93616", null ],
    [ "PLASMA_dsyevr", "group__double_gac40bc729f1349cf26c573be350c92de4.html#gac40bc729f1349cf26c573be350c92de4", null ],
    [ "PLASMA_dsygst", "group__double_ga7ab44c615960d36295086012efc45a7d.html#ga7ab44c615960d36295086012efc45a7d", null ],
    [ "PLASMA_dsygv", "group__double_ga7efdf7cffbb28696c69310d1627a100d.html#ga7efdf7cffbb28696c69310d1627a100d", null ],
    [ "PLASMA_dsygvd", "group__double_ga10b8c70c33dfee0cbbe6acb17217f717.html#ga10b8c70c33dfee0cbbe6acb17217f717", null ],
    [ "PLASMA_dsymm", "group__double_ga1ab6ee29131efe4804f5ff940eeb699a.html#ga1ab6ee29131efe4804f5ff940eeb699a", null ],
    [ "PLASMA_dsyr2k", "group__double_gaf9fa0e62110092a132cd17d8f48561f0.html#gaf9fa0e62110092a132cd17d8f48561f0", null ],
    [ "PLASMA_dsyrk", "group__double_gacfdac462cfb1b2d6696e353bec9fd079.html#gacfdac462cfb1b2d6696e353bec9fd079", null ],
    [ "PLASMA_dsytrd", "group__double_gaebb037c30606f77404ffef513f255d5c.html#gaebb037c30606f77404ffef513f255d5c", null ],
    [ "PLASMA_dTile_to_Lapack", "group__double_ga1a9152c4ac00de6cafe8a5a4ad6718df.html#ga1a9152c4ac00de6cafe8a5a4ad6718df", null ],
    [ "PLASMA_dtrmm", "group__double_ga16876a8b4e9148ba2c8f4d37dca9eef4.html#ga16876a8b4e9148ba2c8f4d37dca9eef4", null ],
    [ "PLASMA_dtrsm", "group__double_ga7dab3739b47f3f77f68cb6166638f507.html#ga7dab3739b47f3f77f68cb6166638f507", null ],
    [ "PLASMA_dtrsmpl", "group__double_ga1ee42a61d4bf0294ed216add96e6ff37.html#ga1ee42a61d4bf0294ed216add96e6ff37", null ],
    [ "PLASMA_dtrsmrv", "group__double_ga01879443f745172ea8165d4882136d36.html#ga01879443f745172ea8165d4882136d36", null ],
    [ "PLASMA_dtrtri", "group__double_ga3968f78f69610b681ab7d6c411f92796.html#ga3968f78f69610b681ab7d6c411f92796", null ]
];