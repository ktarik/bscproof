/**
 *
 * @file core_sgeqp3_norms.c
 *
 *  PLASMA core_blas kernel
 *  PLASMA is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.5.2
 * @author Mark Gates
 * @date 2010-11-15
 * @generated s Mon Sep 16 11:44:06 2013
 *
 **/
#include <lapacke.h>
#include <math.h>
#include <cblas.h>
#include "common.h"

#define A(m,n) BLKADDR( A, float, m, n )

/* temporary definition -- replace when future lapacke includes lassq. */
#ifndef LAPACK_slassq
#define LAPACK_slassq LAPACK_GLOBAL( slassq, SLASSQ )
#endif

void LAPACK_slassq(
    const int *n, const float *x, const int *incx,
    float *scale, float *sumsq );

/***************************************************************************//**
 *
 * @ingroup CORE_float
 *
 *  CORE_sgeqp3_norms computes the 2-norm of each column of A[ ioff:m, joff:n ]
 *  that is marked with norms2[j] == -1 on entry. Entries that are not marked
 *  are assumed to already contain the correct 2-norm, so that the same routine
 *  can be used for computing the initial norms and for updating bad norms.
 *  The result is stored duplicated in norms1 and norms2.
 *
 *******************************************************************************
 *
 *  @param[in] A
 *          PLASMA descriptor of the matrix A.
 *          On entry, the M-by-N matrix described by the descriptor.
 *
 *  @param[in] ioff
 *          Row offset.
 *
 *  @param[in] joff
 *          Column offset.
 *
 *  @param[in,out] norms1
 *          Vector of size A.n.
 *          On exit, norms1[j] is 2-norm of column j, for j >= joff.
 *
 *  @param[in,out] norms2
 *          Vector of size A.n.
 *          On entry, if norms2[j] == -1, re-compute norm of column j.
 *          On exit, norms2[j] is 2-norm of column j, for j >= joff.
 **/
#if defined(PLASMA_HAVE_WEAK)
#pragma weak CORE_sgeqp3_norms = PCORE_sgeqp3_norms
#define CORE_sgeqp3_norms PCORE_sgeqp3_norms
#endif
void CORE_sgeqp3_norms( PLASMA_desc A, int ioff, int joff, float *norms1, float *norms2 )
{
    const float *Ai;
    int j, ii, ioff2, len, mb, nb, lda;
    float sumsq, scale;
    const int ione = 1;
    
    if ( A.nt != 1 ) {
        coreblas_error(1, "Illegal value of A.nt");
        return;
    }

    nb = min( A.nb, A.n );
    for( j = joff; j < nb; ++j ) {
        if ( norms2[j] == -1. ) {
            scale = 0.;
            sumsq = 1.;
            ioff2 = ioff;
            for( ii = 0; ii < A.mt; ++ii ) {
                mb = min( A.mb, A.m - ii*A.mb );
                Ai = A(ii,0);
                lda = BLKLDD( A, ii );
                len = mb - ioff2;
                LAPACK_slassq( &len, &Ai[ioff2 + j*lda], &ione, &scale, &sumsq );
                ioff2 = 0;
            }
            norms2[j] = scale * sqrt( sumsq );
            norms1[j] = norms2[j];
        }
    }
}

/***************************************************************************//**
 *
 **/
void QUARK_CORE_sgeqp3_norms( Quark *quark, Quark_Task_Flags *task_flags,
                              PLASMA_desc A,
                              int ioff, int joff,
                              float *norms1, float *norms2 )
{
    Quark_Task *task;
    int ii, jj;

    DAG_SET_PROPERTIES("norms", "brown");
    task = QUARK_Task_Init( quark, CORE_sgeqp3_norms_quark, task_flags );

    QUARK_Task_Pack_Arg( quark, task, sizeof(PLASMA_desc),  &A,      VALUE          );
    QUARK_Task_Pack_Arg( quark, task, sizeof(int),          &ioff,   VALUE          );
    QUARK_Task_Pack_Arg( quark, task, sizeof(int),          &joff,   VALUE          );
    QUARK_Task_Pack_Arg( quark, task, sizeof(float)*A.nb,  norms1,          INOUT  );
    QUARK_Task_Pack_Arg( quark, task, sizeof(float)*A.nb,  norms2,          INOUT  );

    /* depends on block column */
    /* note: A.nt must be 1. checked in CORE_sgeqp3_norms. */
    for( jj = 0; jj < A.nt; ++jj ) {
        for( ii = 0; ii < A.mt; ++ii ) {
            QUARK_Task_Pack_Arg( quark, task, sizeof(float)*A.mb*A.nb, A(ii,jj), INPUT );
        }
    }

    QUARK_Insert_Task_Packed( quark, task );
}

/***************************************************************************//**
 *
 **/
#if defined(PLASMA_HAVE_WEAK)
#pragma weak CORE_sgeqp3_norms_quark = PCORE_sgeqp3_norms_quark
#define CORE_sgeqp3_norms_quark PCORE_sgeqp3_norms_quark
#endif
void CORE_sgeqp3_norms_quark( Quark *quark )
{
    PLASMA_desc A;
    int ioff, joff;
    float *norms1, *norms2;

    quark_unpack_args_5( quark, A, ioff, joff, norms1, norms2 );
    CORE_sgeqp3_norms(          A, ioff, joff, norms1, norms2 );
}
