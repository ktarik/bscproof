/**
 *
 * @file core_dgeqp3_init.c
 *
 *  PLASMA core_blas kernel
 *  PLASMA is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.5.2
 * @author Mark Gates
 * @date 2010-11-15
 * @generated d Mon Sep 16 11:44:06 2013
 *
 **/
#include <lapacke.h>
#include "common.h"

/***************************************************************************//**
 *
 * @ingroup CORE_double
 *
 *  CORE_dgeqp3_init initializes jpvt to [1, ..., n].
 *  Uses 1-based indexing for Fortran compatability.
 *
 *******************************************************************************
 *
 *  @param[in] n
 *          Size of vector.
 *
 *  @param[in,out] jpvt
 *          Vector of size n.
 *          On exit, jpvt[i] = i+1.
 **/
#if defined(PLASMA_HAVE_WEAK)
#pragma weak CORE_dgeqp3_init = PCORE_dgeqp3_init
#define CORE_dgeqp3_init PCORE_dgeqp3_init
#endif
void CORE_dgeqp3_init( int n, int *jpvt )
{
    int j;
    for( j = 0; j < n; ++j ) {
        jpvt[j] = j+1;
    }
}

/***************************************************************************//**
 *
 **/
void QUARK_CORE_dgeqp3_init( Quark *quark, Quark_Task_Flags *task_flags,
                             int n, int *jpvt )
{
    Quark_Task *task;

    DAG_SET_PROPERTIES("init", "brown");
    task = QUARK_Task_Init( quark, CORE_dgeqp3_init_quark, task_flags );

    QUARK_Task_Pack_Arg( quark, task, sizeof(int),    &n,   VALUE  );
    QUARK_Task_Pack_Arg( quark, task, sizeof(int)*n,  jpvt, OUTPUT );

    QUARK_Insert_Task_Packed( quark, task );
}

/***************************************************************************//**
 *
 **/
#if defined(PLASMA_HAVE_WEAK)
#pragma weak CORE_dgeqp3_init_quark = PCORE_dgeqp3_init_quark
#define CORE_dgeqp3_init_quark PCORE_dgeqp3_init_quark
#endif
void CORE_dgeqp3_init_quark( Quark *quark )
{
    int n;
    int *jpvt;

    quark_unpack_args_2( quark, n, jpvt );
    CORE_dgeqp3_init(           n, jpvt );
}
