/**
 *
 * @file core_slacpy_pivot.c
 *
 *  PLASMA core_blas kernel
 *  PLASMA is a software package provided by Univ. of Tennessee,
 *  Univ. of California Berkeley and Univ. of Colorado Denver
 *
 * @version 2.5.2
 * @author Mathieu Faverge
 * @date 2013-02-01
 * @generated s Mon Sep 16 11:44:07 2013
 *
 **/
#include <lapacke.h>
#include "common.h"

#define A(m, n) BLKADDR(descA, float, m, n)

/***************************************************************************//**
 *
 * @ingroup CORE_float
 *
 *  CORE_slacpy_pivot extracts the original version of the rows selected by the
 *  ipiv array and copies them into a new buffer.
 *
 *  This kernel is used by tournament pivoting algorithms, to extract the
 *  selected rows from the original matrix that will make it to the next level
 *  of the tournament.
 *
 *******************************************************************************
 *
 *  @param[in] descA
 *          The descriptor of the matrix A in which the kernel will extract the
 *          original rows.
 *
 *  @param[in] k1
 *          The first element of IPIV for which a row interchange will
 *          be done.
 *
 *  @param[in] k2
 *          The last element of IPIV for which a row interchange will
 *          be done.
 *
 *  @param[in] ipiv
 *          The pivot indices; Only the element in position k1 to k2
 *          are accessed. The pivots should be included in the interval 1 to A.m
 *
 *  @param[in,out] rankin
 *          On entry, the global indices relative to the full matrix A
 *          factorized, in the local sub-matrix. If init == 1, rankin is
 *          initialized to A.i, .. A.i+descA.m
 *          On exit, rows are permutted according to ipiv.
 *
 *  @param[out] rankout
 *          On exit, contains the global indices of the first (k2-k1+1) rows.
 *
 *  @param[out] A
 *          An LDA-by-descA.n matrix. On exit, A contains the original version
 *          of the rows selected by the pivoting process.
 *
 *  @param[in] LDA
 *          The leading dimension of the array A.  LDA >= max(1,(k2-k1+1)).
 *
 *  @param[in] init
 *          True if rankin needs to be initialized.
 *          False, if rankin contains already initialized data.
 *
 *******************************************************************************
 *
 * @return
 *         \retval PLASMA_SUCCESS successful exit
 *         \retval <0 if INFO = -k, the k-th argument had an illegal value
 *
 ******************************************************************************/
#if defined(PLASMA_HAVE_WEAK)
#pragma weak CORE_slacpy_pivot = PCORE_slacpy_pivot
#define CORE_slacpy_pivot PCORE_slacpy_pivot
#endif
int CORE_slacpy_pivot( const PLASMA_desc descA,
                       PLASMA_enum direct, int k1, int k2, const int *ipiv,
                       int *rankin, int *rankout,
                       float *A, int lda,
                       int init)
{
    int i, ip, it, ir, ld;
    const int *lpiv;
    int *ro = rankout;

    /* Init rankin if first step */
    if ( init ) {
        int val = descA.i;
        for(i=0; i<descA.m; i++, val++) {
            rankin[i] = val;
        }
    }

    /* Generate the rankout */
    ro = rankout;
    lpiv = ipiv;
    for(i=k1-1; i<k2; i++, ro++, lpiv++) {
        *ro = rankin[ (*lpiv) - 1 ];
        rankin[ (*lpiv) - 1 ] = rankin[ i ];
    }

    /* Extract the rows */
    if (direct == PlasmaRowwise) {
       ro = rankout;
       for(i=k1-1; i<k2; i++, ro++) {
           ip = (*ro) - descA.i;
           it = ip / descA.mb;
           ir = ip % descA.mb;
           ld = BLKLDD(descA, it);
           cblas_scopy(descA.n, A(it, 0) + ir, ld,
                                A + i,         lda );
       }
    }
    else {
       ro = rankout;
       for(i=k1-1; i<k2; i++, ro++) {
           ip = (*ro) - descA.i;
           it = ip / descA.mb;
           ir = ip % descA.mb;
           ld = BLKLDD(descA, it);
           cblas_scopy(descA.n, A(it, 0) + ir, ld,
                                A + i*lda,     1 );
       }
    }
    return PLASMA_SUCCESS;
}

/***************************************************************************//**
 *
 **/
void QUARK_CORE_slacpy_pivot(Quark *quark, Quark_Task_Flags *task_flags,
                             const PLASMA_desc descA,
                             PLASMA_enum direct, int k1, int k2, const int *ipiv,
                             int *rankin, int *rankout,
                             float *A, int lda,
                             int pos, int init)
{
    DAG_SET_PROPERTIES( "CPY_PIV"  , "white"   );
    QUARK_Insert_Task(quark, CORE_slacpy_pivot_quark, task_flags,
        sizeof(PLASMA_desc),                    &descA,         VALUE,
        sizeof(PLASMA_enum),                    &direct,        VALUE,
        sizeof(int),                            &k1,            VALUE,
        sizeof(int),                            &k2,            VALUE,
        sizeof(int)*lda,                         ipiv,                INPUT,
        sizeof(int)*lda,                         rankin,              INOUT,
        sizeof(int)*lda,                         rankout,             OUTPUT | GATHERV,
        sizeof(float)*lda*descA.nb, A,                   INOUT | GATHERV,
        sizeof(int),                            &lda,           VALUE,
        sizeof(int),                            &pos,           VALUE,
        sizeof(int),                            &init,          VALUE,
        0);
}

/***************************************************************************//**
 *
 **/
#if defined(PLASMA_HAVE_WEAK)
#pragma weak CORE_slacpy_pivot_quark = PCORE_slacpy_pivot_quark
#define CORE_slacpy_pivot_quark PCORE_slacpy_pivot_quark
#endif
void CORE_slacpy_pivot_quark(Quark *quark)
{
    PLASMA_desc descA;
    PLASMA_enum direct;
    float *A;
    int lda, pos, k1, k2;
    int *rankin;
    int *rankout;
    const int *ipiv;
    int init;

    quark_unpack_args_11(quark, descA, direct, k1, k2, ipiv, rankin, rankout, A, lda, pos, init);
    A = (direct == PlasmaRowwise) ? A + pos : A + (pos * lda);
    CORE_slacpy_pivot(descA, direct, k1, k2, ipiv, rankin, rankout+pos, A, lda, init );
}
