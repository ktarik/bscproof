/**                                                               
 *                                                                
 * @file coreblas_s.c                                             
 *                                                                
 *  PLASMA core_blas tracing kernel                               
 *  PLASMA is a software package provided by Univ. of Tennessee,  
 *  Univ. of California Berkeley and Univ. of Colorado Denver     
 *                                                                
 *  This file provides the wrapper for each function of the       
 *  core_blas library which will generate an event before and     
 *  after the execution of the kernel.                            
 *  This file is automatically generated with convert2eztrace.pl  
 *  script. DO NOT MANUALLY EDIT THIS FILE.                       
 *                                                                
 * @version 2.5.2                                                 
 * @author Mathieu Faverge                                        
 * @date 2010-11-15                                               
 * @generated s Mon Sep 16 11:44:32 2013
 *                                                                
 **/                                                              
#include <eztrace.h>           
#include <ev_codes.h>          
#include "common.h"            
#include "coreblas_ev_codes.h" 
#include "coreblas_macros.h"   
#undef COMPLEX                    
#define REAL                

/*****************************************************************
 *        Core functions                                          
 */

FUNCTION_VOID( CORE_sasum, ASUM, void ,
          (PLASMA_enum storev, PLASMA_enum uplo, int M, int N, const float *A, int lda, float *work),
          (storev, uplo, M, N, A, lda, work) )
FUNCTION_VOID( CORE_sbrdalg1, BRDALG, void ,
          ( PLASMA_enum uplo, int n, int nb, float *A, int lda, float *VQ, float *TAUQ, float *VP, float *TAUP, int Vblksiz, int wantz, int i, int sweepid, int m, int grsiz, float *work),
          (uplo, n, nb, A, lda, VQ, TAUQ, VP, TAUP, Vblksiz, wantz, i, sweepid, m, grsiz, work) )
FUNCTION_TYPE( CORE_sgeadd, GEADD, int ,
          (int M, int N, float alpha, const float *A, int LDA, float *B, int LDB),
          (M, N, alpha, A, LDA, B, LDB) )
FUNCTION_TYPE( CORE_sgelqt, GELQT, int ,
          (int M, int N, int IB, float *A, int LDA, float *T, int LDT, float *TAU, float *WORK),
          (M, N, IB, A, LDA, T, LDT, TAU, WORK) )
FUNCTION_VOID( CORE_sgemm, GEMM, void ,
          (PLASMA_enum transA, int transB, int M, int N, int K, float alpha, const float *A, int LDA, const float *B, int LDB, float beta, float *C, int LDC),
          (transA, transB, M, N, K, alpha, A, LDA, B, LDB, beta, C, LDC) )
FUNCTION_VOID( CORE_sgemm_tile, GEMM, void ,
          (PLASMA_enum transA, PLASMA_enum transB, int m, int n, int k, const float *alpha, const float *A, int lda, const float *B, int ldb, const float *beta, float *C, int ldc ),
          (transA, transB, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc) )
FUNCTION_VOID( CORE_sgemv, GEMV, void ,
          (PLASMA_enum trans, int m, int n, float alpha, const float *A, int lda, const float *x, int incx, float beta, float *y, int incy),
          (trans, m, n, alpha, A, lda, x, incx, beta, y, incy) )
FUNCTION_VOID( CORE_sgemv_tile, GEMV, void ,
          (PLASMA_enum trans, int m, int n, const float *alpha, const float *A, int lda, const float *x, int incx, const float *beta, float *y, int incy ),
          (trans, m, n, alpha, A, lda, x, incx, beta, y, incy) )
FUNCTION_VOID( CORE_sgeqp3_init, GEQP3_INIT, void ,
          ( int n, int *jpvt ),
          (n, jpvt) )
FUNCTION_VOID( CORE_sgeqp3_larfg, LARFG, void ,
          ( PLASMA_desc A, int ii, int jj, int i, int j, float *tau, float *beta ),
          (A, ii, jj, i, j, tau, beta) )
FUNCTION_VOID( CORE_sgeqp3_norms, GEQP3_NORMS, void ,
          ( PLASMA_desc A, int ioff, int joff, float *norms1, float *norms2 ),
          (A, ioff, joff, norms1, norms2) )
FUNCTION_VOID( CORE_sgeqp3_pivot, GEQP3_PIVOT, void ,
          ( PLASMA_desc A, float *F, int ldf, int jj, int k, int *jpvt, float *norms1, float *norms2, int *info ),
          (A, F, ldf, jj, k, jpvt, norms1, norms2, info) )
FUNCTION_TYPE( CORE_sgeqp3_tntpiv, GEQRT, int ,
          (int m, int n, float *A, int lda, int *IPIV, float *tau, int *iwork),
          (m, n, A, lda, IPIV, tau, iwork) )
FUNCTION_VOID( CORE_sgeqp3_update, GEQP3_UPDATE, void ,
          ( const float *Ajj, int lda1, float *Ajk, int lda2, const float *Fk, int ldf, int joff, int k, int koff, int nb, float *norms1, float *norms2, int *info ),
          (Ajj, lda1, Ajk, lda2, Fk, ldf, joff, k, koff, nb, norms1, norms2, info) )
FUNCTION_TYPE( CORE_sgeqrt, GEQRT, int ,
          (int M, int N, int IB, float *A, int LDA, float *T, int LDT, float *TAU, float *WORK),
          (M, N, IB, A, LDA, T, LDT, TAU, WORK) )
FUNCTION_TYPE( CORE_sgessm, GESSM, int ,
          (int M, int N, int K, int IB, const int *IPIV, const float *L, int LDL, float *A, int LDA),
          (M, N, K, IB, IPIV, L, LDL, A, LDA) )
FUNCTION_TYPE( CORE_sgessq, LASSQ, int ,
          (int M, int N, const float *A, int LDA, float *scale, float *sumsq),
          (M, N, A, LDA, scale, sumsq) )
FUNCTION_TYPE( CORE_sgetrf, GETRF, int ,
          (int m, int n, float *A, int lda, int *IPIV, int *info),
          (m, n, A, lda, IPIV, info) )
FUNCTION_TYPE( CORE_sgetrf_incpiv, GETRF, int ,
          (int M, int N, int IB, float *A, int LDA, int *IPIV, int *INFO),
          (M, N, IB, A, LDA, IPIV, INFO) )
FUNCTION_TYPE( CORE_sgetrf_nopiv, GETRF, int ,
          (int M, int N, int IB, float *A, int LDA),
          (M, N, IB, A, LDA) )
FUNCTION_TYPE( CORE_sgetrf_reclap, GETRF, int ,
          (int M, int N, float *A, int LDA, int *IPIV, int *info),
          (M, N, A, LDA, IPIV, info) )
FUNCTION_TYPE( CORE_sgetrf_rectil, GETRF, int ,
          (const PLASMA_desc A, int *IPIV, int *info),
          (A, IPIV, info) )
FUNCTION_VOID( CORE_sgetrip, GETRIP, void ,
          (int m, int n, float *A, float *W) ,
          (m, n, A, W)  )
FUNCTION_VOID( CORE_ssygst, HEGST, void ,
          (int itype, PLASMA_enum uplo, int N, float *A, int LDA, float *B, int LDB, int *INFO),
          (itype, uplo, N, A, LDA, B, LDB, INFO) )
#ifdef COMPLEX
FUNCTION_VOID( CORE_ssymm, HEMM, void ,
          (PLASMA_enum side, PLASMA_enum uplo, int M, int N, float alpha, const float *A, int LDA, const float *B, int LDB, float beta, float *C, int LDC),
          (side, uplo, M, N, alpha, A, LDA, B, LDB, beta, C, LDC) )
#endif
#ifdef COMPLEX
FUNCTION_VOID( CORE_ssyr2k, HER2K, void ,
          (PLASMA_enum uplo, PLASMA_enum trans, int N, int K, float alpha, const float *A, int LDA, const float *B, int LDB, float beta, float *C, int LDC),
          (uplo, trans, N, K, alpha, A, LDA, B, LDB, beta, C, LDC) )
#endif
FUNCTION_TYPE( CORE_ssyrfb, HERFB, int ,
          ( PLASMA_enum uplo, int n, int k, int ib, int nb, const float *A, int lda, const float *T, int ldt, float *C, int ldc, float *WORK, int ldwork ),
          (uplo, n, k, ib, nb, A, lda, T, ldt, C, ldc, WORK, ldwork) )
#ifdef COMPLEX
FUNCTION_VOID( CORE_ssyrk, HERK, void ,
          (PLASMA_enum uplo, PLASMA_enum trans, int N, int K, float alpha, const float *A, int LDA, float beta, float *C, int LDC),
          (uplo, trans, N, K, alpha, A, LDA, beta, C, LDC) )
#endif
#ifdef COMPLEX
FUNCTION_TYPE( CORE_shessq, LASSQ, int ,
          (PLASMA_enum uplo, int N, const float *A, int LDA, float *scale, float *sumsq),
          (uplo, N, A, LDA, scale, sumsq) )
#endif
FUNCTION_VOID( CORE_slacpy, LACPY, void ,
          (PLASMA_enum uplo, int M, int N, const float *A, int LDA, float *B, int LDB),
          (uplo, M, N, A, LDA, B, LDB) )
FUNCTION_TYPE( CORE_slacpy_pivot, LACPY, int ,
          ( const PLASMA_desc descA, PLASMA_enum direct, int k1, int k2, const int *ipiv, int *rankin, int *rankout, float *A, int lda, int init),
          (descA, direct, k1, k2, ipiv, rankin, rankout, A, lda, init) )
FUNCTION_VOID( CORE_slange, LANGE, void ,
          (int norm, int M, int N, const float *A, int LDA, float *work, float *normA),
          (norm, M, N, A, LDA, work, normA) )
#ifdef COMPLEX
FUNCTION_VOID( CORE_slansy, LANHE, void ,
          (int norm, PLASMA_enum uplo, int N, const float *A, int LDA, float *work, float *normA),
          (norm, uplo, N, A, LDA, work, normA) )
#endif
FUNCTION_VOID( CORE_slansy, LANSY, void ,
          (int norm, PLASMA_enum uplo, int N, const float *A, int LDA, float *work, float *normA),
          (norm, uplo, N, A, LDA, work, normA) )
FUNCTION_TYPE( CORE_slarfb_gemm, LARFB, int ,
          (PLASMA_enum side, PLASMA_enum trans, int direct, int storev, int M, int N, int K, const float *V, int LDV, const float *T, int LDT, float *C, int LDC, float *WORK, int LDWORK),
          (side, trans, direct, storev, M, N, K, V, LDV, T, LDT, C, LDC, WORK, LDWORK) )
FUNCTION_VOID( CORE_slaset2, LASET, void ,
          (PLASMA_enum uplo, int M, int N, float alpha, float *A, int LDA),
          (uplo, M, N, alpha, A, LDA) )
FUNCTION_VOID( CORE_slaset, LASET, void ,
          (PLASMA_enum uplo, int M, int N, float alpha, float beta, float *A, int LDA),
          (uplo, M, N, alpha, beta, A, LDA) )
FUNCTION_VOID( CORE_slaswp, LASWP, void ,
          (int N, float *A, int LDA, int I1, int I2, const int *IPIV, int INC),
          (N, A, LDA, I1, I2, IPIV, INC) )
FUNCTION_TYPE( CORE_slaswp_ontile, LASWP, int ,
          (PLASMA_desc descA, int i1, int i2, const int *ipiv, int inc),
          (descA, i1, i2, ipiv, inc) )
FUNCTION_TYPE( CORE_sswptr_ontile, TRSM, int ,
          (PLASMA_desc descA, int i1, int i2, const int *ipiv, int inc, const float *Akk, int ldak),
          (descA, i1, i2, ipiv, inc, Akk, ldak) )
FUNCTION_TYPE( CORE_slaswpc_ontile, LASWP, int ,
          (PLASMA_desc descA, int i1, int i2, const int *ipiv, int inc),
          (descA, i1, i2, ipiv, inc) )
FUNCTION_TYPE( CORE_slatro, LATRO, int ,
          (PLASMA_enum uplo, PLASMA_enum trans, int M, int N, const float *A, int LDA, float *B, int LDB),
          (uplo, trans, M, N, A, LDA, B, LDB) )
FUNCTION_VOID( CORE_slauum, LAUUM, void ,
          (PLASMA_enum uplo, int N, float *A, int LDA),
          (uplo, N, A, LDA) )
FUNCTION_TYPE( CORE_spemv, PEMV, int ,
          (PLASMA_enum trans, int storev, int M, int N, int L, float ALPHA, const float *A, int LDA, const float *X, int INCX, float BETA, float *Y, int INCY, float *WORK),
          (trans, storev, M, N, L, ALPHA, A, LDA, X, INCX, BETA, Y, INCY, WORK) )
#ifdef COMPLEX
FUNCTION_VOID( CORE_splgsy, PLGHE, void ,
          ( float bump, int m, int n, float *A, int lda, int bigM, int m0, int n0, unsigned long long int seed ),
          (bump, m, n, A, lda, bigM, m0, n0, seed) )
#endif
FUNCTION_VOID( CORE_splgsy, PLGSY, void ,
          ( float bump, int m, int n, float *A, int lda, int bigM, int m0, int n0, unsigned long long int seed ),
          (bump, m, n, A, lda, bigM, m0, n0, seed) )
FUNCTION_VOID( CORE_splrnt, PLRNT, void ,
          ( int m, int n, float *A, int lda, int bigM, int m0, int n0, unsigned long long int seed ),
          (m, n, A, lda, bigM, m0, n0, seed) )
FUNCTION_VOID( CORE_spotrf, POTRF, void ,
          (PLASMA_enum uplo, int N, float *A, int LDA, int *INFO),
          (uplo, N, A, LDA, INFO) )
FUNCTION_VOID( CORE_ssetvar, SETVAR, void ,
          (const float *alpha, float *x),
          (alpha, x) )
FUNCTION_VOID( CORE_sshiftw, SHIFTW, void ,
          (int s, int cl, int m, int n, int L, float *A, float *W) ,
          (s, cl, m, n, L, A, W)  )
FUNCTION_VOID( CORE_sshift, SHIFT, void ,
          (int s, int m, int n, int L, float *A) ,
          (s, m, n, L, A)  )
FUNCTION_TYPE( CORE_sssssm, SSSSM, int ,
          (int M1, int N1, int M2, int N2, int K, int IB, float *A1, int LDA1, float *A2, int LDA2, const float *L1, int LDL1, const float *L2, int LDL2, const int *IPIV),
          (M1, N1, M2, N2, K, IB, A1, LDA1, A2, LDA2, L1, LDL1, L2, LDL2, IPIV) )
FUNCTION_VOID( CORE_sswpab, SWPAB, void ,
          (int i, int n1, int n2, float *A, float *work) ,
          (i, n1, n2, A, work)  )
FUNCTION_VOID( CORE_ssymm, SYMM, void ,
          (PLASMA_enum side, PLASMA_enum uplo, int M, int N, float alpha, const float *A, int LDA, const float *B, int LDB, float beta, float *C, int LDC),
          (side, uplo, M, N, alpha, A, LDA, B, LDB, beta, C, LDC) )
FUNCTION_VOID( CORE_ssyr2k, SYR2K, void ,
          (PLASMA_enum uplo, PLASMA_enum trans, int N, int K, float alpha, const float *A, int LDA, const float *B, int LDB, float beta, float *C, int LDC),
          (uplo, trans, N, K, alpha, A, LDA, B, LDB, beta, C, LDC) )
FUNCTION_VOID( CORE_ssyrk, SYRK, void ,
          (PLASMA_enum uplo, PLASMA_enum trans, int N, int K, float alpha, const float *A, int LDA, float beta, float *C, int LDC),
          (uplo, trans, N, K, alpha, A, LDA, beta, C, LDC) )
FUNCTION_TYPE( CORE_ssyssq, LASSQ, int ,
          (PLASMA_enum uplo, int N, const float *A, int LDA, float *scale, float *sumsq),
          (uplo, N, A, LDA, scale, sumsq) )
FUNCTION_VOID( CORE_strdalg1, TRDALG, void ,
          ( int n, int nb, float *A, int lda, float *V, float *TAU, int Vblksiz, int wantz, int i, int sweepid, int m, int grsiz, float *work),
          (n, nb, A, lda, V, TAU, Vblksiz, wantz, i, sweepid, m, grsiz, work) )
FUNCTION_VOID( CORE_strmm, TRMM, void ,
          (PLASMA_enum side, PLASMA_enum uplo, PLASMA_enum transA, PLASMA_enum diag, int M, int N, float alpha, const float *A, int LDA, float *B, int LDB),
          (side, uplo, transA, diag, M, N, alpha, A, LDA, B, LDB) )
FUNCTION_VOID( CORE_strsm, TRSM, void ,
          (PLASMA_enum side, PLASMA_enum uplo, PLASMA_enum transA, PLASMA_enum diag, int M, int N, float alpha, const float *A, int LDA, float *B, int LDB),
          (side, uplo, transA, diag, M, N, alpha, A, LDA, B, LDB) )
FUNCTION_VOID( CORE_strtri, TRTRI, void ,
          (PLASMA_enum uplo, PLASMA_enum diag, int N, float *A, int LDA, int *info),
          (uplo, diag, N, A, LDA, info) )
FUNCTION_TYPE( CORE_stslqt, TSLQT, int ,
          (int M, int N, int IB, float *A1, int LDA1, float *A2, int LDA2, float *T, int LDT, float *TAU, float *WORK),
          (M, N, IB, A1, LDA1, A2, LDA2, T, LDT, TAU, WORK) )
FUNCTION_TYPE( CORE_stsmlq, TSMLQ, int ,
          (PLASMA_enum side, PLASMA_enum trans, int M1, int N1, int M2, int N2, int K, int IB, float *A1, int LDA1, float *A2, int LDA2, const float *V, int LDV, const float *T, int LDT, float *WORK, int LDWORK),
          (side, trans, M1, N1, M2, N2, K, IB, A1, LDA1, A2, LDA2, V, LDV, T, LDT, WORK, LDWORK) )
FUNCTION_TYPE( CORE_stsmlq_corner, TSMLQ, int ,
          ( int m1, int n1, int m2, int n2, int m3, int n3, int k, int ib, int nb, float *A1, int lda1, float *A2, int lda2, float *A3, int lda3, const float *V, int ldv, const float *T, int ldt, float *WORK, int ldwork),
          (m1, n1, m2, n2, m3, n3, k, ib, nb, A1, lda1, A2, lda2, A3, lda3, V, ldv, T, ldt, WORK, ldwork) )
FUNCTION_TYPE( CORE_stsmlq_sytra1, TSMLQ, int ,
          ( PLASMA_enum side, PLASMA_enum trans, int m1, int n1, int m2, int n2, int k, int ib, float *A1, int lda1, float *A2, int lda2, const float *V, int ldv, const float *T, int ldt, float *WORK, int ldwork),
          (side, trans, m1, n1, m2, n2, k, ib, A1, lda1, A2, lda2, V, ldv, T, ldt, WORK, ldwork) )
FUNCTION_TYPE( CORE_stsmqr, TSMQR, int ,
          (PLASMA_enum side, PLASMA_enum trans, int M1, int N1, int M2, int N2, int K, int IB, float *A1, int LDA1, float *A2, int LDA2, const float *V, int LDV, const float *T, int LDT, float *WORK, int LDWORK),
          (side, trans, M1, N1, M2, N2, K, IB, A1, LDA1, A2, LDA2, V, LDV, T, LDT, WORK, LDWORK) )
FUNCTION_TYPE( CORE_stsmqr_corner, TSMQR, int ,
          ( int m1, int n1, int m2, int n2, int m3, int n3, int k, int ib, int nb, float *A1, int lda1, float *A2, int lda2, float *A3, int lda3, const float *V, int ldv, const float *T, int ldt, float *WORK, int ldwork),
          (m1, n1, m2, n2, m3, n3, k, ib, nb, A1, lda1, A2, lda2, A3, lda3, V, ldv, T, ldt, WORK, ldwork) )
FUNCTION_TYPE( CORE_stsmqr_sytra1, TSMQR, int ,
          ( PLASMA_enum side, PLASMA_enum trans, int m1, int n1, int m2, int n2, int k, int ib, float *A1, int lda1, float *A2, int lda2, const float *V, int ldv, const float *T, int ldt, float *WORK, int ldwork),
          (side, trans, m1, n1, m2, n2, k, ib, A1, lda1, A2, lda2, V, ldv, T, ldt, WORK, ldwork) )
FUNCTION_TYPE( CORE_stsqrt, TSQRT, int ,
          (int M, int N, int IB, float *A1, int LDA1, float *A2, int LDA2, float *T, int LDT, float *TAU, float *WORK),
          (M, N, IB, A1, LDA1, A2, LDA2, T, LDT, TAU, WORK) )
FUNCTION_TYPE( CORE_ststrf, TSTRF, int ,
          (int M, int N, int IB, int NB, float *U, int LDU, float *A, int LDA, float *L, int LDL, int *IPIV, float *WORK, int LDWORK, int *INFO),
          (M, N, IB, NB, U, LDU, A, LDA, L, LDL, IPIV, WORK, LDWORK, INFO) )
FUNCTION_TYPE( CORE_sttlqt, TTLQT, int ,
          (int M, int N, int IB, float *A1, int LDA1, float *A2, int LDA2, float *T, int LDT, float *TAU, float *WORK),
          (M, N, IB, A1, LDA1, A2, LDA2, T, LDT, TAU, WORK) )
FUNCTION_TYPE( CORE_sttmlq, TTMLQ, int ,
          (PLASMA_enum side, PLASMA_enum trans, int M1, int N1, int M2, int N2, int K, int IB, float *A1, int LDA1, float *A2, int LDA2, const float *V, int LDV, const float *T, int LDT, float *WORK, int LDWORK),
          (side, trans, M1, N1, M2, N2, K, IB, A1, LDA1, A2, LDA2, V, LDV, T, LDT, WORK, LDWORK) )
FUNCTION_TYPE( CORE_sttmqr, TTMQR, int ,
          (PLASMA_enum side, PLASMA_enum trans, int M1, int N1, int M2, int N2, int K, int IB, float *A1, int LDA1, float *A2, int LDA2, const float *V, int LDV, const float *T, int LDT, float *WORK, int LDWORK),
          (side, trans, M1, N1, M2, N2, K, IB, A1, LDA1, A2, LDA2, V, LDV, T, LDT, WORK, LDWORK) )
FUNCTION_TYPE( CORE_sttqrt, TTQRT, int ,
          (int M, int N, int IB, float *A1, int LDA1, float *A2, int LDA2, float *T, int LDT, float *TAU, float *WORK),
          (M, N, IB, A1, LDA1, A2, LDA2, T, LDT, TAU, WORK) )
FUNCTION_TYPE( CORE_sormlq, UNMLQ, int ,
          (PLASMA_enum side, PLASMA_enum trans, int M, int N, int K, int IB, const float *A, int LDA, const float *T, int LDT, float *C, int LDC, float *WORK, int LDWORK),
          (side, trans, M, N, K, IB, A, LDA, T, LDT, C, LDC, WORK, LDWORK) )
FUNCTION_TYPE( CORE_sormqr, UNMQR, int ,
          (PLASMA_enum side, PLASMA_enum trans, int M, int N, int K, int IB, const float *A, int LDA, const float *T, int LDT, float *C, int LDC, float *WORK, int LDWORK),
          (side, trans, M, N, K, IB, A, LDA, T, LDT, C, LDC, WORK, LDWORK) )

/*****************************************************************
 *        QUARK Wrapper functions                                 
 */

FUNCTION_QUARK( CORE_sasum_quark, ASUM )
FUNCTION_QUARK( CORE_sasum_f1_quark, ASUM )
FUNCTION_QUARK( CORE_sbrdalg1_quark, BRDALG )
FUNCTION_QUARK( CORE_sgeadd_quark, GEADD )
FUNCTION_QUARK( CORE_sgelqt_quark, GELQT )
FUNCTION_QUARK( CORE_sgemm_quark, GEMM )
FUNCTION_QUARK( CORE_sgemm_f2_quark, GEMM )
FUNCTION_QUARK( CORE_sgemm_p2_quark, GEMM )
FUNCTION_QUARK( CORE_sgemm_p3_quark, GEMM )
FUNCTION_QUARK( CORE_sgemm_p2f1_quark, GEMM )
FUNCTION_QUARK( CORE_sgemm_tile_quark, GEMM )
FUNCTION_QUARK( CORE_sgemv_quark, GEMV )
FUNCTION_QUARK( CORE_sgemv_tile_quark, GEMV )
FUNCTION_QUARK( CORE_sgeqp3_init_quark, GEQP3_INIT )
FUNCTION_QUARK( CORE_sgeqp3_larfg_quark, LARFG )
FUNCTION_QUARK( CORE_sgeqp3_norms_quark, GEQP3_NORMS )
FUNCTION_QUARK( CORE_sgeqp3_pivot_quark, GEQP3_PIVOT )
FUNCTION_QUARK( CORE_sgeqp3_tntpiv_quark, GEQRT )
FUNCTION_QUARK( CORE_sgeqp3_update_quark, GEQP3_UPDATE )
FUNCTION_QUARK( CORE_sgeqrt_quark, GEQRT )
FUNCTION_QUARK( CORE_sgessm_quark, GESSM )
FUNCTION_QUARK( CORE_sgessq_quark, LASSQ )
FUNCTION_QUARK( CORE_sgessq_f1_quark, LASSQ )
FUNCTION_QUARK( CORE_sgetrf_quark, GETRF )
FUNCTION_QUARK( CORE_sgetrf_incpiv_quark, GETRF )
FUNCTION_QUARK( CORE_sgetrf_nopiv_quark, GETRF )
FUNCTION_QUARK( CORE_sgetrf_reclap_quark, GETRF )
FUNCTION_QUARK( CORE_sgetrf_rectil_quark, GETRF )
FUNCTION_QUARK( CORE_sgetrip_quark, GETRIP )
FUNCTION_QUARK( CORE_sgetrip_f1_quark, GETRIP )
FUNCTION_QUARK( CORE_sgetrip_f2_quark, GETRIP )
FUNCTION_QUARK( CORE_ssygst_quark, HEGST )
#ifdef COMPLEX
FUNCTION_QUARK( CORE_ssymm_quark, HEMM )
#endif
#ifdef COMPLEX
FUNCTION_QUARK( CORE_ssyr2k_quark, HER2K )
#endif
FUNCTION_QUARK( CORE_ssyrfb_quark, HERFB )
#ifdef COMPLEX
FUNCTION_QUARK( CORE_ssyrk_quark, HERK )
#endif
#ifdef COMPLEX
FUNCTION_QUARK( CORE_shessq_quark, LASSQ )
FUNCTION_QUARK( CORE_shessq_f1_quark, LASSQ )
#endif
FUNCTION_QUARK( CORE_slacpy_quark, LACPY )
FUNCTION_QUARK( CORE_slacpy_f1_quark, LACPY )
FUNCTION_QUARK( CORE_slacpy_pivot_quark, LACPY )
FUNCTION_QUARK( CORE_slange_quark, LANGE )
FUNCTION_QUARK( CORE_slange_f1_quark, LANGE )
#ifdef COMPLEX
FUNCTION_QUARK( CORE_slansy_quark, LANHE )
FUNCTION_QUARK( CORE_slansy_f1_quark, LANHE )
#endif
FUNCTION_QUARK( CORE_slansy_quark, LANSY )
FUNCTION_QUARK( CORE_slansy_f1_quark, LANSY )
FUNCTION_QUARK( CORE_slaset2_quark, LASET )
FUNCTION_QUARK( CORE_slaset_quark, LASET )
FUNCTION_QUARK( CORE_slaswp_quark, LASWP )
FUNCTION_QUARK( CORE_slaswp_f2_quark, LASWP )
FUNCTION_QUARK( CORE_slaswp_ontile_quark, LASWP )
FUNCTION_QUARK( CORE_slaswp_ontile_f2_quark, LASWP )
FUNCTION_QUARK( CORE_sswptr_ontile_quark, TRSM )
FUNCTION_QUARK( CORE_slaswpc_ontile_quark, LASWP )
FUNCTION_QUARK( CORE_slatro_quark, LATRO )
FUNCTION_QUARK( CORE_slatro_f1_quark, LATRO )
FUNCTION_QUARK( CORE_slauum_quark, LAUUM )
#ifdef COMPLEX
FUNCTION_QUARK( CORE_splgsy_quark, PLGHE )
#endif
FUNCTION_QUARK( CORE_splgsy_quark, PLGSY )
FUNCTION_QUARK( CORE_splrnt_quark, PLRNT )
FUNCTION_QUARK( CORE_splssq_quark, LASSQ )
FUNCTION_QUARK( CORE_spotrf_quark, POTRF )
FUNCTION_QUARK( CORE_ssetvar_quark, SETVAR )
FUNCTION_QUARK( CORE_sshiftw_quark, SHIFTW )
FUNCTION_QUARK( CORE_sshift_quark, SHIFT )
FUNCTION_QUARK( CORE_sssssm_quark, SSSSM )
FUNCTION_QUARK( CORE_sswpab_quark, SWPAB )
FUNCTION_QUARK( CORE_ssymm_quark, SYMM )
FUNCTION_QUARK( CORE_ssyr2k_quark, SYR2K )
FUNCTION_QUARK( CORE_ssyrk_quark, SYRK )
FUNCTION_QUARK( CORE_ssyssq_quark, LASSQ )
FUNCTION_QUARK( CORE_ssyssq_f1_quark, LASSQ )
FUNCTION_QUARK( CORE_strdalg1_quark, TRDALG )
FUNCTION_QUARK( CORE_strmm_quark, TRMM )
FUNCTION_QUARK( CORE_strmm_p2_quark, TRMM )
FUNCTION_QUARK( CORE_strsm_quark, TRSM )
FUNCTION_QUARK( CORE_strtri_quark, TRTRI )
FUNCTION_QUARK( CORE_stslqt_quark, TSLQT )
FUNCTION_QUARK( CORE_stsmlq_quark, TSMLQ )
FUNCTION_QUARK( CORE_stsmlq_corner_quark, TSMLQ )
FUNCTION_QUARK( CORE_stsmlq_sytra1_quark, TSMLQ )
FUNCTION_QUARK( CORE_stsmqr_quark, TSMQR )
FUNCTION_QUARK( CORE_stsmqr_corner_quark, TSMQR )
FUNCTION_QUARK( CORE_stsmqr_sytra1_quark, TSMQR )
FUNCTION_QUARK( CORE_stsqrt_quark, TSQRT )
FUNCTION_QUARK( CORE_ststrf_quark, TSTRF )
FUNCTION_QUARK( CORE_sttlqt_quark, TTLQT )
FUNCTION_QUARK( CORE_sttmlq_quark, TTMLQ )
FUNCTION_QUARK( CORE_sttmqr_quark, TTMQR )
FUNCTION_QUARK( CORE_sttqrt_quark, TTQRT )
FUNCTION_QUARK( CORE_sormlq_quark, UNMLQ )
FUNCTION_QUARK( CORE_sormqr_quark, UNMQR )

