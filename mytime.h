#ifndef __DAYTIME_H_
#define __DAYTIME_H_


/*
typedef struct timeval {
  long tv_sec;
  long tv_usec;
} timeval;
*/

struct timezone 
{
  int  tz_minuteswest; /* minutes W of Greenwich */
  int  tz_dsttime;     /* type of dst correction */
};

int gettimeofday(struct timeval *tv, struct timezone *tz);
#endif