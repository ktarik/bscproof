
/******************************************************************************/
/* @brief: Matrix multiplication on the CPU with randomly generated matrices  */
/*		   A * B = C														  */
/* @author: TKR																  */
/*																			  */
/******************************************************************************/
 
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// Thread block size
//#define BLOCK_SIZE 3
//#include "MatrixMulKernel.cu"

#define WIDTHA 3   
#define WIDTHB 3   
#define WIDTHC WB  

#define HEIGHTA 3   
#define HEIGHTB WIDTHA  
#define HEIGHTC HEIGHTA  


// Allocates a matrix with random float entries.
void randomMatInit(float* data, int size)
{
	int i = 0;
    for (i = 0; i < size; ++i)
        data[i] = rand() / (float)RAND_MAX;
}
 

void matrixMulOnHost( float* M, float* N, float* R, int Width) {
	for (int i = 0; i < Width; ++i)
		for (int j = 0; j < Width; ++j) {
			float sum = 0;
			for (int k = 0; k < Width; ++k) {
				float a = M[i * Width + k];
				float b = N[k * Width + j];
				sum += a * b;
			}
			R[i * Width + j] = sum;
		}
}


int main(int argc, char** argv)
{
 
    // set seed for rand()
    srand(9999);
 
    // 1. allocate host memory for matrices A and B
    unsigned int size_A = WA * HA;
    unsigned int mem_size_A = sizeof(float) * size_A;
    float* h_A = (float*) malloc(mem_size_A);
 
    unsigned int size_B = WB * HB;
    unsigned int mem_size_B = sizeof(float) * size_B;
    float* h_B = (float*) malloc(mem_size_B);
 
    // 2. initialize host memory
    randomMatInit(h_A, size_A);
    randomMatInit(h_B, size_B);
  
    // 3. print out A and B
    printf("\n\nMatrix A\n");
    for(int i = 0; i < size_A; i++)
    {
       printf("%f ", h_A[i]);
       if(((i + 1) % WA) == 0)
          printf("\n");
    }
 
    printf("\n\nMatrix B\n");
    for(int i = 0; i < size_B; i++)
    {
       printf("%f ", h_B[i]);
       if(((i + 1) % WB) == 0)
          printf("\n");
    }
 
    // 4. allocate host memory for the result C
    unsigned int size_C = WC * HC;
    unsigned int mem_size_C = sizeof(float) * size_C;
    float* h_C = (float*) malloc(mem_size_C);
    
    // 5. perform the calculation
    MatrixMulOnHost( h_A, h_B, h_C, WA );
 
    // 6. print out the results
    printf("\n\nMatrix C (Results)\n");
    for(int i = 0; i < size_C; i++)
    {
       printf("%f ", h_C[i]);
       if(((i + 1) % WC) == 0)
          printf("\n");
    }
    printf("\n");
 
    // 7. clean up memory
    free(h_A);
    free(h_B);
    free(h_C);
 
}