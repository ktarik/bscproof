/*
** utils.c -  set of utility routines
** Copyright (C) 1999-2000 Ricardo Bonon <bonon@opp.com.br>
****************************************************************************
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston,
** MA 02111-1307, USA.
**  
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifndef WIN32
#include <unistd.h>
#endif

 
 

/***************************************************************************
strsplit :  searchs a string 'str' for a character 'ch' and split it in two 
            parts, a suffix (the part after the character including it) and a 
            prefix (the part before the character).  The function returns the 
            prefix if the argument 'part' is 'p' and the suffix if it is 's',
            otherwise it returns NULL. The function can search 'str' forwards 
            or backward, if the argument direction is 'f' or 'b' respectively.
***************************************************************************/
char *strsplit(char *str, char ch, char part,char direction){
  
   char *suffix;
   char *prefix;
   int diff;

   if (direction=='b'){
      if ((suffix=strrchr(str,ch))==(char*)NULL){
         return (str);
      }
   }
   else if (direction=='f'){
      if ((suffix=strchr(str,ch))==NULL){
         return (str);
      }
   }
   diff=strlen(str)-strlen(suffix);
   prefix=(char*)malloc(diff*sizeof(char));
   strncpy(prefix,str,diff*sizeof(char));
   if (part=='p') return(prefix);
   else if (part=='s') return(suffix);
   else return ((char*)NULL);
}


/***************************************************************************
read_i : Reads an integer value from an ASCII file, ignoring coments
***************************************************************************/
int read_i(FILE *in, int *numb)
{
 char dum, dummy[128];

 for(;;)
  {fscanf(in,"%s", dummy);
   if(dummy[0]=='#' && strlen(dummy)>1 && dummy[strlen(dummy)-1]=='#') {}
   else if(dummy[0]=='#') {do{fscanf(in,"%c", &dum);} while(dum!='#');}
   else                   {*numb=atoi(dummy); break;} }
}

/***************************************************************************
read_f : Reads a float value from an ASCII file, ignoring coments
***************************************************************************/
int read_f(FILE *in, float *numb)
{
 char dum, dummy[128];

 for(;;)
  {fscanf(in,"%s", dummy);
   if(dummy[0]=='#' && strlen(dummy)>1 && dummy[strlen(dummy)-1]=='#') {}
   else if(dummy[0]=='#') {do{fscanf(in,"%c", &dum);} while(dum!='#');}
   else                   {*numb=atof(dummy); break;} }
}

/***************************************************************************
read_d : Reads a double value from an ASCII file, ignoring coments
***************************************************************************/
int read_d(FILE *in, double *numb)
{
 char dum, dummy[128];

 for(;;)
  {fscanf(in,"%s", dummy);
   if(dummy[0]=='#' && strlen(dummy)>1 && dummy[strlen(dummy)-1]=='#') {}
   else if(dummy[0]=='#') {do{fscanf(in,"%c", &dum);} while(dum!='#');}
   else                   {*numb=(double)atof(dummy); break;} }
}


/***************************************************************************
read_s : Reads a string value from an ASCII file, ignoring coments
***************************************************************************/
int read_s(FILE *in, char *string)
{                  
 char dum, dummy[128];

 for(;;)
  {fscanf(in,"%s", dummy);
   if(dummy[0]=='#' && strlen(dummy)>1 && dummy[strlen(dummy)-1]=='#') {}
   else if(dummy[0]=='#') {do{fscanf(in,"%c", &dum);} while(dum!='#');}
   else                   {strcpy(string, dummy); break;} }
}




