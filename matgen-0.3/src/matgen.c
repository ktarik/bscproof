/*
** matgen.c -  matrix generator in MatrixMarket file format
** Copyright (C) 2000 Ricardo Bonon <bonon@opp.com.br>
****************************************************************************
**  
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**  
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**  
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston,
** MA 02111-1307, USA.
**  
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <unistd.h>
#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "utils.h"
#include "mmio.h"


/***************************************************************************
 FUNCTION PROTOTYPES
***************************************************************************/
unsigned int MT_RANDSEED();                       /* generate random seed */
float        MT_RANDNUM (unsigned int *);       /* random number generator*/
int          MT_RANDOM (int, char *);     /*random sparse matrix generator*/
int          MT_ROWRANDOM (int, char *);  /*random by row matrix generator*/


/***************************************************************************
 GLOBAL VARS
***************************************************************************/
int GENERATE_LOG=1;


/***************************************************************************
 MAIN FUNCTION
***************************************************************************/
int
main (int argc, char *argv[])
{


  char mt_inputfilename[128];
  char *mt_usage =
   "matgen 0.3.1: generates matrices in MatrixMarket file format\n\
   \b\b\bPorted to Win32 by TKR\n\
   \b\b\bCopyright (C) 2000,2001 Ricardo Bonon (bonon@users.sourceforge.net)\n\n\
   \b\busage : matgen inputfile (for input from file) \n\
   \b\b        matgen           (for input from stdin)\n\
   \b\bman matgen for more info\n";
  FILE * mt_input;

  int mt_type;

  /* DEBUG TESTING */
  //argc = 2;
 



  /*This arg parsing is quite rudimentar. This will be rewritten with getopt()
    in future versions*/
  if ((argc==2)
      &&(
          (strcmp(argv[1],"-h")==0)
          ||(strcmp(argv[1],"--help")==0)
          ||(strcmp(argv[1],"-v")==0)
          ||(strcmp(argv[1],"--version")==0)
        )
      )
    {
        fprintf (stderr, "%s\r\n", mt_usage);
        exit (1);
    }

  if (argc > 3)
    {
      fprintf (stderr, "%s\r\n", mt_usage);
      exit (1);
    }

  if (argc == 1)
    {
         //strcpy(mt_inputfilename,(char*)NULL);
       // read_i (stdin, &mt_type);
		fprintf(stderr, "%s\r\n", mt_usage );
		fprintf(stderr, "No file found");
		exit(1);
   }
  else
    {
        /*A turn-around to make it possible to not generate a log file. All of this will be
          rewriten with getopt*/
        if (argc == 3)
          {
              /*Check if -n option is used. This option avoid log-file generation*/
              if (strcmp(argv[1],"-n")==0)
                {
                    GENERATE_LOG=0;
                }
              argv[1]=argv[2];

          }

        /*Open input file for reading */
        strcpy (mt_inputfilename, argv[1]);
		//strcpy (mt_inputfilename, "test003.input"); //DEBUG TESTING
		
        if ((mt_input = fopen (mt_inputfilename, "r")) == NULL)
          {
              fprintf (stderr,
                       "matgen error: could not open file %s for reading\r\nTry -h for help\r\n",
                       mt_inputfilename);
              exit (1);
          }
        read_i (mt_input, &mt_type);
        fclose (mt_input);
    }

  /*Generate matrices */
  switch (mt_type)
    {
    case 0:
      MT_RANDOM (argc, mt_inputfilename);
      break;
    case 1:
      MT_ROWRANDOM (argc, mt_inputfilename);
      break;
    default:
      break;
    }
  return (0);
}



/***************************************************************************
 MT_RANDSEED:  Get random seed for Random number generator according
               using /dev/random
***************************************************************************/
unsigned int
MT_RANDSEED ()
{
    int fd;                            /*Variable for reading /dev/random */
	unsigned int seed;
#ifndef WIN32
    fd=open("/dev/random", O_RDONLY);
    read(fd, &seed, sizeof(unsigned int));
    seed=abs(seed);
    close(fd);
    return (seed);
#else
	return time(0);
#endif

}



/***************************************************************************
 MT_RANDNUM:  Random number generator according to "Numerical Recipes in C",
              2nd Ed., page 279
***************************************************************************/
float
MT_RANDNUM (unsigned int *idum)
{
  long k;
  float ans;
#define IA 16807
#define IM 2147483647
#define AM (1.0/IM)
#define IQ 127773
#define IR 2836
#define MASK 123459876

  *idum ^= MASK;
  k = (*idum) / IQ;
  *idum = IA * (*idum - k * IQ) - IR * k;
  if (*idum < 0)
    *idum += IM;
  ans = AM * (*idum);
  *idum ^= MASK;
  return ans;
}


/***************************************************************************
 MT_RANDOM: generate a random sparse matrix in MatrixMarket file format
***************************************************************************/
int MT_RANDOM (int argcount, char *inputfilename)
{

  struct mt_entry
  {
    int row;
    int col;
    double value;
    int done;
  }*mt_matrix;			/*Struct for the matrix elements */


  FILE * input,			/*Pointer to input file */
    *log;			/*Pointer to log file */

  char *logfilename;

  double mt_minimum,
    mt_maximum,			/*Limits for element value */

    mt_diagmultfactor,		/*Multiplication factor for
				   diagonal elements in diagonal
				   dominant matrices */

    mt_density,			/*Non-zero density */

    val;			/*General purpose value
				   (usually for reading) */


   int mt_diagdominant,		/*Flag for diagonal dominant */

    mt_nzdiagonal,		/*Flag for Non-zero diagonal */

    mt_count,
    mt_count2,
    mt_nzcount,
    i,
    j,
    nzmin,
    nzmax,			/*Counters */

    mt_nonzeros,		/*Total number of nonzeros */

    mt_symmetry,		/*Flag for symmetry */

    mt_foundrepeated,		/*Flag to check for repeated
				   entries */

    mt_entryfound,		/*Flag to force printing output
				   in     dense format */

    mt_definitesys,		/*Flag to check if system if
				   definite */

   *mt_nzperrow,		/*Array of non-zeros per row */

    mt_forcesparse,		/*Force output in sparse format */

    mt_numdiags,
    mt_numrows,
    mt_numcols;			/*Matrix dimensions */

  unsigned int seed;            /*Seed for integer random numbers */

  MM_typecode code;		/*MatrixMarket code for matrix */

  time_t mt_now;		/*Time */

  long mt_nowl;


  if (argcount==1) /* Reading from stdin */
    {
        /*Read input file variables */
        read_i (stdin, &mt_symmetry);
        read_i (stdin, &mt_numrows);
        read_i (stdin, &mt_numcols);
        read_d (stdin, &mt_density);
        read_i (stdin, &mt_nzdiagonal);
        read_i (stdin, &mt_diagdominant);
        read_d (stdin, &mt_diagmultfactor);
        read_d (stdin, &mt_minimum);
        read_d (stdin, &mt_maximum);
        read_i (stdin, &mt_forcesparse);
    }
  else /* Reading from input file */
    {
        /*Finding log file name */
        logfilename =
            (char *) malloc ((strlen (inputfilename) + 10) * sizeof (char));
        strcpy (logfilename, inputfilename);
        strcat (logfilename, ".log");

        /*Open log file */
        if (GENERATE_LOG)
          {
              if ((log = fopen (logfilename, "w")) == NULL)
                {
                    fprintf (stderr,
                             "matgen error: could not open file %s for writing\r\n",
                             logfilename);
                    exit (1);
                }
          }
        /*Open input file */
        if ((input = fopen (inputfilename, "r")) == NULL)
          {
              fprintf (stderr,
                       "matgen error: could not open file %s for reading\r\nTry -h for help\r\n",
                       inputfilename);
              exit (1);
          }

        /*Skip matrix type value on input file */
        read_i (input, &i);

        /*Read input file variables */
        read_i (input, &mt_symmetry);
        read_i (input, &mt_numrows);
        read_i (input, &mt_numcols);
        read_d (input, &mt_density);
        read_i (input, &mt_nzdiagonal);
        read_i (input, &mt_diagdominant);
        read_d (input, &mt_diagmultfactor);
        read_d (input, &mt_minimum);
        read_d (input, &mt_maximum);
        read_i (input, &mt_forcesparse);
    }

  if ((mt_symmetry != 0) && (mt_symmetry != 1))
    {
      fprintf (stderr, "matgen error: %d = invalid matrix ", mt_symmetry);
      fprintf (stderr, "symmetry value (should be 0 or 1)\r\n");
      exit (1);
    }

  if (mt_numrows <= mt_numcols)
    mt_numdiags = mt_numrows;
  else
    mt_numdiags = mt_numcols;

  /*Calculates number of non-zeros from density and size */
  /*Multiplication done to eliminate rounding errors */
  mt_density = mt_density * 1.000001;
  mt_nonzeros = ((double) mt_density * mt_numrows * mt_numcols);

  /*Allocating matrix structure */
  mt_matrix = (struct mt_entry *) malloc (mt_nonzeros * sizeof (struct
								mt_entry));
  for (i = 0; i < mt_nonzeros; i++)
    {
      mt_matrix[i].row = 0;
      mt_matrix[i].col = 0;
      mt_matrix[i].done = 0;
      mt_matrix[i].value = 0.0;
    }
    /*** Symmetric matrix, yet to be implemented!!! ***/
  if (mt_symmetry)
    {
      /*Non-zero diagonal */
      if (mt_nzdiagonal)
	{
	  /*Diagonal dominant */
	  if (mt_diagdominant != 0)
	    {
	    }
	}
      /*Random sparse matrix */
      else
	{
	}
    }
    /*** Nonsymmetric matrix ***/
  else
    {
      /*Non-zero diagonal */
      if (mt_nzdiagonal)
	{
	  /*Diagonal dominant */
	  if (mt_diagdominant)
	    {
	      /*initializing random seeds */
              seed=MT_RANDSEED();  
	      srand (seed);

	      mt_count = 0;
	      mt_nzcount = 0;
	      /*Main loop of matrix generation */
	      for (mt_count = 0; mt_count < (mt_nonzeros - mt_numdiags);
		   mt_count++)
		{
		  do
		    {
		      /*Generating random indices i,j */
		      mt_foundrepeated = 0;
		      i = 0 + (int) (((mt_numrows - 1) - 0 + 1) *
                                     ((float)rand ()/(float) RAND_MAX));
		      j =
			0 +
			(int) (((mt_numcols - 1) - 0 + 1) *
			       ((float) rand () / (float) RAND_MAX));
		      /*Check if the pair i,j was already generated. The
		         checking is done from 0 to mt_nzcount which is the
		         number of non-zeros generated untill now. If i==j
		         the flag is also set, for we will generate the
		         diagonal elements later */
		      for (mt_count2 = 0; mt_count2 < mt_nzcount;
			   mt_count2++)
			{
			  if (((i == mt_matrix[mt_count2].row)
			       && (j == mt_matrix[mt_count2].col)
			       && (mt_matrix[mt_count2].done)) || (i == j))
			    /*If pair was already generated, set
			       flag */
			    mt_foundrepeated = 1;
			}
		      /*repeat untill i,j is a new pair */
		    }
		  while (mt_foundrepeated);
		  mt_matrix[mt_count].row = i;
		  mt_matrix[mt_count].col = j;
		  mt_matrix[mt_count].value = (mt_minimum + \
					       (mt_maximum -
						mt_minimum) *
					       MT_RANDNUM (&seed));
		  mt_matrix[mt_count].done = 1;
		  mt_nzcount++;
		}
	      /*Generating the diagonal elements. They are the sum the
	         of all elements in the row multiplied by mt_diagmultfactor */
	      i = mt_nonzeros - mt_numdiags;
	      for (mt_count = 0; mt_count < mt_numdiags; mt_count++)
		{
		  mt_matrix[i + mt_count].row = mt_count;
		  mt_matrix[i + mt_count].col = mt_count;
		  val = 0;
		  for (j = 0; j < mt_nonzeros - mt_numdiags; j++)
		    {
		      if (mt_matrix[j].row == mt_count)
			val = val + fabs (mt_matrix[j].value);
		    }
		  if (val == 0)
		    val = (mt_minimum + (mt_maximum - mt_minimum)
			   * MT_RANDNUM (&seed));
		  mt_matrix[i + mt_count].value = val * mt_diagmultfactor;
		  mt_matrix[i + mt_count].done = 1;
		}
	    }
	  else
	    {
	      /*initializing random seeds */
              seed=MT_RANDSEED();  
	      srand (seed);

              mt_count = 0;
	      for (mt_count = 0; mt_count < (mt_nonzeros - mt_numdiags);
		   mt_count++)
		{
		  do
		    {
		      mt_foundrepeated = 0;
		      i = 0 + (int) (((mt_numrows - 1) - 0 + 1) * (
								   (float)
								   rand ()
								   /
								   (float)
								   RAND_MAX));
		      j =
			0 +
			(int) (((mt_numcols - 1) - 0 + 1) *
			       ((float) rand () / (float) RAND_MAX));
		      for (mt_count2 = 0; mt_count2 < mt_nonzeros;
			   mt_count2++)
			{
			  if (((i == mt_matrix[mt_count2].row)
			       && (j == mt_matrix[mt_count2].col)
			       && (mt_matrix[mt_count2].done)) || (i == j))
			    mt_foundrepeated = 1;
			}
		    }
		  while (mt_foundrepeated);
		  mt_matrix[mt_count].row = i;
		  mt_matrix[mt_count].col = j;
		  mt_matrix[mt_count].value = (mt_minimum + \
					       (mt_maximum -
						mt_minimum) *
					       MT_RANDNUM (&seed));
		  mt_matrix[mt_count].done = 1;
		}
	      i = mt_nonzeros - mt_numdiags;
	      for (mt_count = 0; mt_count < mt_numdiags; mt_count++)
		{
		  mt_matrix[i + mt_count].row = mt_count;
		  mt_matrix[i + mt_count].col = mt_count;
		  val =
		    (mt_minimum +
		     (mt_maximum - mt_minimum) * MT_RANDNUM (&seed));
		  mt_matrix[i + mt_count].value = val;
		  mt_matrix[i + mt_count].done = 1;
		}
	    }
	}
      /*Random sparse matrix */
      else
	{
          /*Reading random seed as an unsigned int from /dev/randomd*/
          seed=MT_RANDSEED();
	  srand (seed);
	  mt_count = 0;
	  for (mt_count = 0; mt_count < mt_nonzeros; mt_count++)
	    {
	      do
		{
		  mt_foundrepeated = 0;
		  i = 0 + (int) (((mt_numrows - 1) - 0 + 1) * (
							       (float)
							       rand () /
							       (float)
							       RAND_MAX));
		  j =
		    0 +
		    (int) (((mt_numcols - 1) - 0 + 1) *
			   ((float) rand () / (float) RAND_MAX));
		  for (mt_count2 = 0; mt_count2 < mt_nonzeros; mt_count2++)
		    {
		      if ((i == mt_matrix[mt_count2].row) &&
			  (j == mt_matrix[mt_count2].col) &&
			  (mt_matrix[mt_count2].done))
			mt_foundrepeated = 1;
		    }
		}
	      while (mt_foundrepeated);
	      mt_matrix[mt_count].row = i;
	      mt_matrix[mt_count].col = j;
	      mt_matrix[mt_count].value = (mt_minimum + \
					   (mt_maximum -
					    mt_minimum) * MT_RANDNUM (&seed));
	      mt_matrix[mt_count].done = 1;
	    }
	}
    }
  time (&mt_now);
  mt_nzperrow = (int *) malloc (mt_numrows * sizeof (int));
  /*Counting non-zeros per row */
  for (i = 0; i < mt_numrows; i++)
    {
      mt_nzperrow[i] = 0;
      mt_count = 0;
      for (j = 0; j < mt_nonzeros; j++)
	{
	  if ((mt_matrix[j].row == i) && (mt_matrix[j].done))
	    mt_count++;
	}
      mt_nzperrow[i] = mt_count;
    }
  /*Checking for lines with only zeros (indefinite system) */
  mt_definitesys = 1;
  for (i = 0; i < mt_numrows; i++)
    {
      if (mt_nzperrow[i] == 1)
	mt_definitesys = 0;
    }

  /*Finding max and min number of non-zeros per row */
  nzmin = mt_nzperrow[1];
  nzmax = mt_nzperrow[1];
  for (i = 0; i < mt_numrows; i++)
    {
      if (mt_nzperrow[i] < nzmin)
	nzmin = mt_nzperrow[i];
      if (mt_nzperrow[i] > nzmax)
	nzmax = mt_nzperrow[i];
    }
  if (nzmin == 1)
    nzmin = 0;
  if (nzmax == 1)
    nzmax = 0;

  if (GENERATE_LOG)
    {
        /*Writing log info to log file */
        fprintf (log, "Generated at %s", ctime (&mt_now));
        fprintf (log, "by matgen (C) 2000 Ricardo Bonon (bonon@users.sourceforge.net)\r\n\r\n");

        fprintf (log, "input file: \t\t\"%s\"\r\n", inputfilename);
        fprintf (log, "number of rows: \t%d\r\n", mt_numrows);
        fprintf (log, "number of cols: \t%d\r\n", mt_numrows);
        if (mt_density > 1e-04)
            fprintf (log, "non-zero density:\t%0.5f (%d non-zeros)\r\n", mt_density,\
                     mt_nonzeros);
        else
            fprintf (log, "non-zero density:\t%0.5e (%d non-zeros)\r\n", mt_density,\
                     mt_nonzeros);
        fprintf (log, "min non-zeros per row :\t%d\r\n", nzmin);
        fprintf (log, "max non-zeros per row :\t%d\r\n\r\n", nzmax);
        if (mt_definitesys)
          {
              fprintf (log, "DEFINITE SYSTEM : non-zeros at all rows\r\n");
              fprintf (log, "Non-zeros per row\r\n", i);
              for (i = 0; i < mt_numrows; i++)
                {
                    fprintf (log, "\tRow %d: %d\r\n", i + 1, mt_nzperrow[i]);
                }
          }
        else
          {
              fprintf (log, "INDEFINITE SYSTEM: SOME ROWS WITH ONLY ZEROS!\r\n");
              for (i = 0; i < mt_numrows; i++)
                {
                    if (mt_nzperrow[i] == 1)
                        fprintf (log, "\tROW %d\r\n", i + 1);
                }
          }
    }
  /*Writing output to stdout */
  /*Initializes the MM info struct variable. See MM documentation. */
  mm_initialize_typecode (&code);
  /*Set matrix info */
  mm_set_matrix (&code);
  /*Set matrix as real */
  mm_set_real (&code);
  /*Set matrix as general */
  mm_set_general (&code);
  /*If density > 1/3, set matrix as dense, else it is sparse */
  if ((mt_density > (1.0 / 3.0)) && (!mt_forcesparse))
    mm_set_array (&code);
  else
    mm_set_coordinate (&code);
  /*Writes MM banner info to file */
  mm_write_banner (stdout, code);
  /*Gets time */
  time (&mt_now);
  /*Writes header comments to file: time, matrix size, density */
  printf ( "%%\r\n%% Generated at %s", ctime (&mt_now));
  printf (
	   "%% by matgen (C) 2000 Ricardo Bonon (bonon@users.sourceforge.net)\n%%\n");
  if (GENERATE_LOG)
    {
        printf ( "%% \n%% from file \"%s\"\r\n", inputfilename);
    }
  printf ( "%% \tMatrix type:\t\t random sparse\r\n");
  printf ( "%% \tNumber of rows:\t\t %d \r\n", mt_numrows);
  printf ( "%% \tNumber of columns:\t %d \r\n", mt_numcols);
  if (mt_density > 1e-04)
    printf ( "%% \tNon-zero density:\t %0.5f\r\n", mt_density);
  else
    printf ( "%% \tNon-zero density:\t %0.5e\r\n", mt_density);
  printf ( "%% \tNumber of non-zeros:\t %d\r\n", mt_nonzeros);
  if (mt_nzdiagonal)
    printf ( "%% \tNon-zero diagonal:\t yes\r\n");
  else
    printf ( "%% \tNon-zero diagonal:\t no\r\n");
  if (mt_diagdominant)
    {
      printf ( "%% \tDiagonal dominant:\t yes\r\n");
      if (mt_diagmultfactor > 1e-04)
	printf ( "%% \tDiagonal mult. factor:\t% 0.5f\r\n",
		 mt_diagmultfactor);
      else
	printf ( "%% \tDiagonal mult. factor:\t% 0.5e\r\n",
		 mt_diagmultfactor);
    }
  else
    printf ( "%% \tDiagonal dominant:\t no\r\n");

  if (mt_minimum > 1e-04)
    printf ( "%% \tMinimum value:\t\t %0.5f\r\n", mt_minimum);
  else
    printf ( "%% \tMinimum value:\t\t %0.5e\r\n", mt_minimum);
  if (mt_maximum > 1e-04)
    printf ( "%% \tMaximum value:\t\t %0.5f\r\n", mt_maximum);
  else
    printf ( "%% \tMaximum value:\t\t %0.5e\r\n", mt_maximum);

  if (mt_definitesys)
    {
      printf ( "%% \tDefinite system : non-zeros at all rows\r\n");
      if (GENERATE_LOG)
        {
            printf ( "%% \tSee log file \"%s\" for details\r\n",
                     logfilename);
        }
    }
  else
    {
      printf (
	       "%% \tINDEFINITE SYSTEM: SOME ROWS WITH ONLY ZEROS!\r\n");
      printf (
	       "%% \tIncrease non-zero density to obtain a definite system\r\n");
      if (GENERATE_LOG)
        {
            printf ( "%% \tSee log file \"%s\" for details\r\n",logfilename);
        }
    }
  printf ( "%%\r\n");
  /*Write MM size info */
  if ((mt_density > (1.0 / 3.0)) && (!mt_forcesparse))
    {
      mm_write_mtx_array_size (stdout, mt_numrows, mt_numcols);
    }
  else
    {
      mm_write_mtx_crd_size (stdout, mt_numrows, mt_numcols, mt_nonzeros);
    }
  /*Write matrix in dense (array) format if density < 1/3 and if
     user do not force sparse output */
  if ((mm_is_dense (code)) && (!mt_forcesparse))
    {
      for (i = 0; i < mt_numrows; i++)
	{
	  for (j = 0; j < mt_numcols; j++)
	    {
	      mt_entryfound = 0;
	      for (mt_count = 0; mt_count < mt_nonzeros; mt_count++)
		{
		  if ((mt_matrix[mt_count].row == i) &&
		      (mt_matrix[mt_count].col == j) && (!mt_entryfound))
		    {
		      printf ("%f\r\n", mt_matrix[mt_count].value);
		      mt_entryfound = 1;
		    }
		}
	      if (!mt_entryfound)
		{
		  printf ("%f\r\n", 0.0);
		}
	    }
	}
    }
  /*Else, write it in sparse (coordinate) format */
  else
    {
      for (mt_count = 0; mt_count < mt_nonzeros; mt_count++)
	{
	  printf ("%d %d %0.8f\r\n", mt_matrix[mt_count].row + 1,
		  mt_matrix[mt_count].col + 1, mt_matrix[mt_count].value);
	}
      i = mt_nonzeros - mt_numrows;
    }
  fclose (input);
  if (GENERATE_LOG)
    {
        fclose (log);
    }
  return (0);
}


/***************************************************************************
 MT_ROWRANDOM:  generate a random sparse matrix row by row,  with n elements
                in each row in MatrixMarket file format
***************************************************************************/
int
MT_ROWRANDOM (int argcount, char *inputfilename)
{


  struct mt_entry
  {
    int row;
    int col;
    double value;
    int done;
  }
   *mt_matrix;			/*Struct for the matrix elements */


  FILE * input,			/*Pointer to input file */
    *log;			/*Pointer to log file */

  char *logfilename;

  double mt_minimum,
    mt_maximum,			/*Limits for elements values */

    mt_diagmultfactor,		/*Multiplication factor for
				   diagonal elements in diagonal
				   dominant matrices */

    mt_density;			/*Non-zero density */

  int mt_diagdominant,		/*Flag for diagonal dominant */

    mt_nzdiagonal,		/*Flag for Non-zero diagonal */

    mt_count,
    i,
    j,
    k,
    mt_nzcount,
    mt_count2,			/*Counters */

    mt_nzmin,
    mt_nzmax,			/*Limits for number of non-zeros
				   per row */

    mt_nonzeros,		/*Total of non-zeros in matrix */

    mt_definitesys,		/*Flag to check if system if
				   definite */

    mt_isrepeated,		/*Flag to search for repeated
				   elements in matrix */

   *mt_nzperrow,		/*Array of non-zeros per row */

    mt_forcesparse,		/*Force output in sparse format */

    mt_numdiags,		/*Number of diagonal elements */

    mt_entryfound,		/*Flag used when writing output
				   in dense format */

    mt_numrows,
    mt_numcols;			/*Matrix dimensions */


  unsigned int seed;		/*Seed for integer random numbers */

  MM_typecode code;		/*MatrixMarket code for matrix */

  time_t mt_now;		/*Time var for ctime() */

  long mt_nowl;			/*Long time var for random seed */


  if (argcount==1) /* Reading from stdin */
    {
        read_i (stdin, &mt_numrows);
        read_i (stdin, &mt_numcols);
        read_i (stdin, &mt_nzmax);
        read_i (stdin, &mt_nzmin);
        read_i (stdin, &mt_nzdiagonal);
        read_i (stdin, &mt_diagdominant);
        read_d (stdin, &mt_diagmultfactor);
        read_d (stdin, &mt_minimum);
        read_d (stdin, &mt_maximum);
        read_i (stdin, &mt_forcesparse);
    }
  else
    {
        /*Finding log file name */
        logfilename =
            (char *) malloc ((strlen (inputfilename) + 10) * sizeof (char));
        strcpy (logfilename, inputfilename);
        strcat (logfilename, ".log");

        if (GENERATE_LOG)
          {
              /*Open log file */
              if ((log = fopen (logfilename, "w")) == NULL)
                {
                    fprintf (stderr,
                             "matgen error: could not open file %s for writing\r\n",
                             logfilename);
                    exit (1);
                }
          }
        /*Open input file */
        if ((input = fopen (inputfilename, "r")) == NULL)
          {
              fprintf (stderr,
                       "matgen error: could not open file %s for reading\r\nTry -h for help\r\n",
                       inputfilename);
              exit (1);
          }

        /*Skip matrix type value on input file */
        read_i (input, &i);

        /*Read input file variables */
        read_i (input, &mt_numrows);
        read_i (input, &mt_numcols);
        read_i (input, &mt_nzmax);
        read_i (input, &mt_nzmin);
        read_i (input, &mt_nzdiagonal);
        read_i (input, &mt_diagdominant);
        read_d (input, &mt_diagmultfactor);
        read_d (input, &mt_minimum);
        read_d (input, &mt_maximum);
        read_i (input, &mt_forcesparse);
    }
        
  /*Calculating number of diagonals */
  if (mt_numrows <= mt_numcols)
    mt_numdiags = mt_numrows;
  else
    mt_numdiags = mt_numcols;

  /*Checking input errors */
  if (mt_nzmax > mt_numcols)
    {
      fprintf (stderr, "matgen error: error in non-zeros per line");
      fprintf (stderr, " limits (max>num_columns)\r\n");
      exit (1);
    }
  if (mt_nzmax < mt_nzmin)
    {
      fprintf (stderr, "matgen error: error in non-zeros per line");
      fprintf (stderr, " limits (max<min)\r\n");
      exit (1);
    }
  if (mt_nzmax < 0)
    {
      fprintf (stderr, "matgen error: error in non-zeros per line");
      fprintf (stderr, " limits (max<0)\r\n");
      exit (1);
    }
  if (mt_nzmin < 0)
    {
      fprintf (stderr, "matgen error: error in non-zeros per line");
      fprintf (stderr, " limits (min<0)\r\n");
      exit (1);
    }
  if ((mt_diagdominant != 0) && (mt_nzdiagonal == 0))
    {
      fprintf (stderr, "matgen error: diagonal dominant requires non-");
      fprintf (stderr, "zero diagonal\r\n");
      exit (1);
    }
  if (mt_maximum < mt_minimum)
    {
      fprintf (stderr, "matgen error: error in values limits (max<min)\r\n");
      exit (1);
    }

  /*Generating number of non-zeros per row, from random values between
     mt_nzmax and mt_nzmin */
  if ((mt_nzperrow = (int *) malloc (mt_numrows * sizeof (int))) == NULL)
    {
      fprintf (stderr, "matgen error: memory allocation error\r\n");
      exit (1);
    }

  /*initializing random seeds */
  seed=MT_RANDSEED();
  srand (seed);
  for (i = 0; i < mt_numrows; i++)
    {
      mt_nzperrow[i] =
	mt_nzmin +
	(int) ((mt_nzmax - mt_nzmin + 1) *
	       ((float) rand () / (float) RAND_MAX));
    }
  /*Correcting non-zeros per row to account for diagonals. If matrix
     is non-zero digagonal, it will count only off-diagonal elements */
  for (i = 0; i < mt_numrows; i++)
    {
      if ((i < mt_numdiags) && (mt_nzdiagonal))
	{
	  mt_nzperrow[i] -= 1;
	  if (mt_nzperrow[i] < 0)
	    mt_nzperrow[i] = 0;
	}
    }

  /*Counting non-zeros */
  mt_nonzeros = 0;
  for (i = 0; i < mt_numrows; i++)
    {
      mt_nonzeros += mt_nzperrow[i];
    }
  if (mt_nzdiagonal)
    mt_nonzeros += mt_numdiags;

  /*Calculating non-zero density */
  mt_density =
    ((double) mt_nonzeros / ((double) mt_numrows * (double) mt_numcols));


  /*Allocating and initializing matrix structure */
  if ((mt_matrix = (struct mt_entry *) malloc (mt_nonzeros * sizeof (struct
								     mt_entry)))
      == NULL)
    {
      fprintf (stderr, "matgen error: memory allocation error\r\n");
      exit (1);
    }
  for (i = 0; i < mt_nonzeros; i++)
    {
      mt_matrix[i].row = 0;
      mt_matrix[i].col = 0;
      mt_matrix[i].done = 0;
      mt_matrix[i].value = 0.0;
    }

  /*Generating matrix elements */

  /*Non-zero diagonal matrix */
  if (mt_nzdiagonal)
    {
      /*initializing random seeds */
      seed=MT_RANDSEED();  
      srand (seed);
      mt_nzcount = 0;
      mt_count2 = 0;
      /*Row loop */
	  for (i = 0; i < mt_numrows; i++)
	  {
		  /*For each row, generates nzperrow[i] off-diagonal non-zeros
		  (we have already excluded the diagonal elements when counting
		  non-zeros), and force <i> to be different of <j> */
		  for (mt_count = 0; mt_count < mt_nzperrow[i]; mt_count++)
		  {
			  do
			  {
				  mt_isrepeated = 0;
				  /*Random column */
				  j = (int) ((mt_numcols) * ((float) rand () /
					  (float) RAND_MAX));
				  
				  /*Check if pair i,j hasn't been generated */
				  /*
				  printf("Checking if pair (%d,%d) hasn't been generated\n", i, j );
				  for (k = mt_count2; k < mt_nzcount; k++)
				  {
					  if ((mt_matrix[k].row == i)
						  && (mt_matrix[k].col == j)
						  && (mt_matrix[k].done))
					  {
						  mt_isrepeated = 1;
					  }

				  }
				  */
			  } while (mt_isrepeated || (i == j));


			  /*i,j are ok, assign values to element */
			  mt_matrix[mt_nzcount].row = i;
			  mt_matrix[mt_nzcount].col = j;
			  mt_matrix[mt_nzcount].done = 1;
			  mt_matrix[mt_nzcount].value = (mt_minimum + \
				  (mt_maximum -
				  mt_minimum) *
				  MT_RANDNUM (&seed));
			  mt_nzcount++;
		  }
		  /*mt_count2 points the adress of the first element of the
		  row in mt_matrix */
		  mt_count2 += mt_nzperrow[i];
	  }
      /*Now, the diagonals */
      mt_count = 0;
      for (i = 0; i < mt_numdiags; i++)
	{
	  mt_nzperrow[i] += 1;
	  mt_matrix[mt_nzcount].row = i;
	  mt_matrix[mt_nzcount].col = i;
	  mt_matrix[mt_nzcount].done = 1;

	  /*if matrix is diagonal dominant */
	  if (mt_diagdominant)
	    {
	      for (j = mt_count; j < (mt_count + mt_nzperrow[i] - 1); j++)
		{
		  mt_matrix[mt_nzcount].value += fabs (mt_matrix[j].value);
		}
	      /*Diagonal = (mt_diagmultfactor * sum of the row) */
	      mt_matrix[mt_nzcount].value *= mt_diagmultfactor;
	    }
	  /*If it is not diag dominant, diagonal=random value */
	  else
	    mt_matrix[mt_nzcount].value = (mt_minimum + \
					   (mt_maximum -
					    mt_minimum) * MT_RANDNUM (&seed));

	  /*mt_nzcount is the index for mt_matrix */
	  mt_nzcount++;
	  /*mt_count is the index to the first element of the
	     row in mt_matrix */
	  mt_count = mt_count + mt_nzperrow[i] - 1;
	}
    }
  /*Random sparse matrix */
  else
    {
      /*initializing random seeds */
      seed=MT_RANDSEED();  
      srand (seed);
      mt_nzcount = 0;
      mt_count2 = 0;
      /*Row loop */
      for (i = 0; i < mt_numrows; i++)
	{
	  /*For each row, generates nzperrow[i] non-zeros (we
	     exclude 1 because the diagonal elements will be
	     generated later */
	  for (mt_count = 0; mt_count < mt_nzperrow[i]; mt_count++)
	    {
	      do
		{
		  mt_isrepeated = 0;
		  /*Random column */
		  j = (int) ((mt_numcols) * ((float) rand () /
					     (float) RAND_MAX));
		  /*Check if pair i,j hasn't been generated */
		  for (k = mt_count2; k < mt_nzcount; k++)
		    {
		      if ((mt_matrix[k].row == i)
			  && (mt_matrix[k].col == j)
			  && (mt_matrix[k].done))
			{
			  mt_isrepeated = 1;
			}

		    }
		}
	      while (mt_isrepeated);
	      /*i,j are ok, assign values to element */
	      mt_matrix[mt_nzcount].row = i;
	      mt_matrix[mt_nzcount].col = j;
	      mt_matrix[mt_nzcount].done = 1;
	      mt_matrix[mt_nzcount].value = (mt_minimum + \
					     (mt_maximum -
					      mt_minimum) *
					     MT_RANDNUM (&seed));
	      mt_nzcount++;
	    }
	  /*mt_count2 points the adress of the first element of the
	     row in mt_matrix */
	  mt_count2 += mt_nzperrow[i] - 1;
	}
    }
  mt_definitesys = 1;
  for (i = 0; i < mt_numrows; i++)
    {
      if (mt_nzperrow[i] < 2)
	mt_definitesys = 0;
    }

  if (GENERATE_LOG)
    {
        fprintf (log, "Generated at %s", ctime (&mt_now));
        fprintf (log, "by matgen (C) 2000 Ricardo Bonon (bonon@users.sourceforge.net)\r\n\r\n");
        fprintf (log, "input file: \t\t\"%s\"\r\n", inputfilename);
        fprintf (log, "number of rows: \t%d\r\n", mt_numrows);
        fprintf (log, "number of cols: \t%d\r\n", mt_numrows);
        fprintf (log, "min non-zeros per row :\t%d\r\n", mt_nzmin);
        fprintf (log, "max non-zeros per row :\t%d\r\n", mt_nzmax);
        if (mt_density > 1e-04)
            fprintf (log, "non-zero density:\t%0.5f (%d non-zeros)\r\n\r\n",
                     mt_density, mt_nonzeros);
        else
            fprintf (log, "non-zero density:\t%0.5e (%d non-zeros)\r\n\r\n",
                     mt_density, mt_nonzeros);

        if (mt_definitesys)
          {
              fprintf (log, "DEFINITE SYSTEM : non-zeros at all rows\r\n");
              fprintf (log, "Non-zeros per row\r\n", i);
              for (i = 0; i < mt_numrows; i++)
                {
                    fprintf (log, "\tRow %d: %d\r\n", i + 1, mt_nzperrow[i]);
                }
          }
        else
          {
              fprintf (log, "INDEFINITE SYSTEM: SOME ROWS WITH ONLY ZEROS!\r\n");
              for (i = 0; i < mt_numrows; i++)
                {
                    if (mt_nzperrow[i] == 1)
                        fprintf (log, "\tROW %d\r\n", i + 1);
                }
          }
    }

  /*Initializes the MM info struct variable. See MM documentation. */
  mm_initialize_typecode (&code);
  /*Set matrix info */
  mm_set_matrix (&code);
  /*Set matrix as real */
  mm_set_real (&code);
  /*Set matrix as general */
  mm_set_general (&code);
  /*If density > 1/3, set matrix as dense, else it is sparse */
  if ((mt_density > (1.0 / 3.0)) && (!mt_forcesparse))
    mm_set_array (&code);
  else
    mm_set_coordinate (&code);
  /*Writes MM banner info to file */
  mm_write_banner (stdout, code);
  /*Gets time */
  time (&mt_now);
  /*Writes header comments to file: time, matrix size, density */
  printf ( "%%\r\n%% Generated at %s", ctime (&mt_now));
  printf (
	   "%% by matgen (C) 2000 Ricardo Bonon (bonon@users.sourceforge.net)");
  printf ( "%% \r\n%%\r\n%% from file \"%s\"\r\n", inputfilename);
  printf ( "%% \tMatrix type:\t\t random sparse by row\r\n");
  printf ( "%% \tNumber of rows:\t\t %d \r\n", mt_numrows);
  printf ( "%% \tNumber of columns:\t %d \r\n", mt_numcols);

  if (mt_density > 1e-04)
    printf ( "%% \tNon-zero density:\t %0.5f\r\n", mt_density);
  else
    printf ( "%% \tNon-zero density:\t %0.5e\r\n", mt_density);

  printf ( "%% \tNumber of non-zeros:\t %d\r\n", mt_nonzeros);
  printf ( "%% \tmin non-zeros per row:\t %d\r\n", mt_nzmin);
  printf ( "%% \tmax non-zeros per row:\t %d\r\n", mt_nzmax);

  if (mt_nzdiagonal)
    printf ( "%% \tNon-zero diagonal:\t yes\r\n");
  else
    printf ( "%% \tNon-zero diagonal:\t no\r\n");
  if (mt_diagdominant)
    {
      printf ( "%% \tDiagonal dominant:\t yes\r\n");
      if (mt_diagmultfactor > 1e-04)
	printf ( "%% \tDiagonal mult. factor:\t% 0.5f\r\n",
		 mt_diagmultfactor);
      else
	printf ( "%% \tDiagonal mult. factor:\t% 0.5e\r\n",
		 mt_diagmultfactor);
    }
  else
    printf ( "%% \tDiagonal dominant:\t no\r\n");

  if (mt_minimum > 1e-04)
    printf ( "%% \tMinimum value:\t\t %0.5f\r\n", mt_minimum);
  else
    printf ( "%% \tMinimum value:\t\t %0.5e\r\n", mt_minimum);
  if (mt_maximum > 1e-04)
    printf ( "%% \tMaximum value:\t\t %0.5f\r\n", mt_maximum);
  else
    printf ( "%% \tMaximum value:\t\t %0.5e\r\n", mt_maximum);

  if (mt_definitesys)
    {
      printf ( "%% \tDefinite system : non-zeros at all rows\r\n");
      printf ( "%% \tSee log file \"%s\" for details\r\n",
	       logfilename);
    }
  else
    {
      printf (
	       "%% \tINDEFINITE SYSTEM: SOME ROWS WITH ONLY ZEROS!\r\n");
      printf (
	       "%% \tIncrease non-zero density to obtain a definite system\r\n");
      printf ( "%% \tSee log file \"%s\" for details\r\n",
	       logfilename);
    }
  printf ( "%%\r\n");

  /*Write MM size info */
  if ((mt_density > (1.0 / 3.0)) && (!mt_forcesparse))
    {
      mm_write_mtx_array_size (stdout, mt_numrows, mt_numcols);
    }
  else
    {
      mm_write_mtx_crd_size (stdout, mt_numrows, mt_numcols, mt_nonzeros);
    }
  /*Write matrix in dense (array) format */
  if ((mm_is_dense (code)) && (!mt_forcesparse))
    {
      for (i = 0; i < mt_numrows; i++)
	{
	  for (j = 0; j < mt_numcols; j++)
	    {
	      mt_entryfound = 0;
	      for (mt_count = 0; mt_count < mt_nonzeros; mt_count++)
		{
		  if ((mt_matrix[mt_count].row == i) &&
		      (mt_matrix[mt_count].col == j) && (!mt_entryfound))
		    {
		      printf ("%f\r\n", mt_matrix[mt_count].value);
		      mt_entryfound = 1;
		    }
		}
	      if (!mt_entryfound)
		{
		  printf ("%f\r\n", 0.0);
		}
	    }
	}
    }
  /*Else, write it in sparse (coordinate) format */
  else
    {
      for (mt_count = 0; mt_count < mt_nonzeros; mt_count++)
	{
	  printf ("%d %d %0.8f\r\n", mt_matrix[mt_count].row + 1,
		  mt_matrix[mt_count].col + 1, mt_matrix[mt_count].value);
	}
      i = mt_nonzeros - mt_numrows;
    }
  fclose (input);
  if (GENERATE_LOG)
    {
        fclose (log);
    }
  return (0);
}
