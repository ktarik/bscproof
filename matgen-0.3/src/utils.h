/***************************************************************************
                          utils.h  -  description                              
                             -------------------                                         
    begin                : Fri Jul 09 1999                                           
    copyright            : (C) 1999 by Ricardo Bonon                         
    email                : prb@iris.ufscar.br                                     
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   * 
 *                                                                         *
 ***************************************************************************/

#include <stdio.h>

int   read_i(FILE *, int *);
int   read_f(FILE *, float *);               
int   read_d(FILE *, double *);               
int   read_s(FILE *, char *);
char  *strsplit(char *, char, char,char);






