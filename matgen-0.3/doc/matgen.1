.\" Copyright (C) 2000,2001 Ricardo Bonon <bonon@users.sourceforge.net>
.\" All rights reserved.
.\"
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions
.\" are met:
.\" 1. Redistributions of source code must retain the above copyright
.\"    notice, this list of conditions and the following disclaimer.
.\" 2. Redistributions in binary form must reproduce the above copyright
.\"    notice, this list of conditions and the following disclaimer in the
.\"    documentation and/or other materials provided with the distribution.
.\" 3. All advertising materials mentioning features or use of this software
.\"    must display the following acknowledgement:
.\"	This product includes software developed by Ricardo Bonon.
.\"
.\" THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR 
.\" IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
.\" IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
.\" ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
.\" FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
.\" DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
.\" OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
.\" HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
.\" LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
.\" OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
.\" SUCH DAMAGE.
.\"
.\"     @(#)matgen		0.3 05/20/2001
.\"
.\"
.TH matgen 1  "20 May 2001" "" ""
.SH NAME
matgen \- a random sparse matrix generator in MatrixMarket file format
.SH VERSION
.P 
This is the manpage for matgen 0.3
.SH SYNOPSIS
.B matgen 
.I inputfile 
.sp
.SH DESCRIPTION
.B matgen
generates random sparse matrices to 
.B
stdout
in 
.I
MatrixMarket
file format taking information from an 
.B input file
with the properties of the matrix to be generated.
.PP
It can generate the following types of matrices: 
.TP
.B random sparse matrix
Matrices where the user specify a non-zero density (among other properties) 
and 
.B matgen
generates the matrix randomly.

.TP
.B random sparse matrix by row
Matrices where the user specify numbers of non-zeros per row (among other 
properties) 
and 
.B matgen
generates the matrix row by row

.P
Bellow is a description of the input files and the options for each matrix type.


.SH MATRIX TYPES
.SH Random sparse matrices
.P
For this type of matrices, user must input a desired non-zero density, among other 
inputs, and the program generates non-zero elements until this density is achieved.
The generation can take a long time for large matrices (above 5000x5000), for the 
program does a strict verification to avoid the generation of repeated entries. which 
is time consuming due to the way the program stores the matrix (in condensed row format),
 to save memory.

The input file for matrices of this type is as follow. Strings starting and ending 
with a 
.B # 
character (including multilines strings) are treated as comments.

.P
   # Matrix type #
   0
   #Symmetry:  	(0 - non-symmetric / 1 - symmetric)#
   0
   #Number of rows#
   10000
   #Number of cols#
   10000
   #Non-zero density#
   0.0007
   #Non-zero diagonal (0 - no / 1 - yes)#
   1
   #diagonal dominant (0 - no / 1 - yes)#
   1
   #diagonal multiplication factor#
   1.0
   #minimum#
   0
   #maximum#
   1
   #force sparse output format  (0 - no / 1 - yes)#
   1

.TP
.B matrix type
0 for random sparse matrices. This value tells the program to generate a random 
sparse matrix.

.TP
.B symmetry
This flag is not yet implemented. But in some future release it will be working.

.TP
.B number of rows & cols
Number of rows and cols of the matrix to be generated.

.TP
.B non-zero density
This value controls the number of non-zeros to be generated. It is defined as being
num_non-zeros/(numrows*numcols).

.TP
.B non-zero diagonal
Flag to control whether to generate a matrix with a non-zeros diagonal or not. If this
flag is 1 and diagonal-dominant is 0, the diagonal elements will have random values
as the other non-zero elements.

.TP
.B diagonal dominant
Flag to control whether to generate a diagonal dominant matrix, a matrix in which the 
diagonal elements are equal or greater than the sum of the absolute values of the 
elements in its row.

.TP
.B diagonal multiplication factor
Value which will multiply the sum of absolute values for elements of a row in 
diagonal dominant matrices. The diagonal elements will be calculated by:

diag_element=(diag_multiplication_fact)*(sum_abs_values)

.TP
.B minimum & maximum
Limits for the random values to be generated.

.TP
.B force sparse output format
Use this flag to force output in sparse format. This is useful if you
want to visualize the generated matrix with some matrix viewer, for some of 
them only read input in MatrixMarket sparse format. 

For a very good matrix viewer see James Kohl's
.B MatView 
at http://www.epm.ornl.gov/~kohl/MatView


.SH Random sparse matrices by row
This type of matrix is generated 
.B *VERY* 
much faster. The user must input limits 
for random numbers of non-zeros per row. For each row, the program generates a 
random number of non-zeros, and generates the non-zeros values, checking for repeated 
elements line by line, which causes it to be much faster than in the previous type.

For matrices with non-zero diagonal, the diagonal element is accounted in the counting 
of non-zero values per row, so the row has exactly the number of non-zeros specified. 

Symmetry is not, and may never be, implemented in this type of matrix. This type is useful 
to generate matrices similar to the ones which arise from the discretisation of PDEs over
triangular non-structured meshes. The random number of non-zeros per row can be tunned
to be similar to the number of neighbors each node has in such meshes. (Typical values are
from 2 to 10).

The input file for matrices of this type is as follow. Strings starting and ending 
with a 
.B # 
character (including multilines strings) are treated as comments.

   #Type#
   1	
   #Number of rows#
   5000
   #Number of cols#
   5000
   #max non-zeros per row#
   10
   #min non-zeros per row#
   2
   #Non-zero diagonal (0 - no / 1 - yes)#
   1
   #diagonal dominant (0 - no / 1 - yes)#
   1
   #diagonal multiplication factor#
   1.1
   #minimum#
   0
   #maximum#
   1
   #force sparse output format  (0 - no / 1 - yes)#
   1

.TP
.B matrix type
1 for random sparse matrices by row. This value tells the program to generate a random 
sparse matrix by row.

.TP
.B max & min non-zeros per row
Limits for the random number of non-zeros per row.

.P
The other inputs are the same as for the
.B random sparse
matrix.

.SH "BUGS"
.P
As this software is under development, you can find some bugs. Please send any 
comments, suggestions or bug reports to the author at bonon@users.sourceforge.net
.P
A know serious problem is when you want to generate matrices with a high non-zero 
density for which the program spend a lot of time checking for repeated entries. 
This should not be a problem since only sparse matrices are supported by now. But
in some cases, i.e. one-column matrices for the right-hand-side of linear systems,
a high non-zero density is desirable, and the matrix generation can last a long time.
.P
This will be fixed in future versions by the addition of algorithms specially designed
to support matrices with one small dimension (rows or columns).


.SH "COPYRIGHT"
.PP
Copyright (C)  2000,2001 Ricardo Bonon. 
.PP
No guarantees or warranties or anything are provided or implied in any
way whatsoever. Use this program at your own risk. Permission to use
this program for any purpose is given, as long as the copyright is
kept intact.
.BI "matgen"
is distributed under the terms of the GNU Publishing License.

.SH "AUTHORS"
.PP
Ricardo Bonon <bonon@users.sourceforge.net>
.PP
If you find 
.BI matgen
useful, please drop me a note.

