#
# SPEC file for matgen-0.3
# COPYRIGHT (C) 2000,2001 Ricardo Bonon <bonon@users.sourceforge.net>
#
%define version 0.3


summary: Matrix generator in MatrixMarket file format
name: matgen
version: 0.3
Release: 1
copyright: GPL
group: Applications/Math
url: http://matgen.sourceforge.net
source: matgen-0.3.tar.gz
vendor: Ricardo Bonon
packager: Ricardo Bonon <bonon@users.sourceforge.net>

%description
Matgen is a command line matrix generator. It uses an input file describing 
the features of the matrix and writes it to stdout as an ASCII-formatted
stream according to the MatrixMarket file format. 
It currently supports random sparse matrices. 


%prep

%setup

%build
./configure
make


%install
make install


%files
/usr/local/bin/matgen
/usr/local/man/man1/matgen.1
/usr/local/share/matgen/mat01.input
/usr/local/share/matgen/mat02.input
/usr/local/share/matgen/mat03.input
/usr/local/share/matgen/mat04.input
/usr/local/share/matgen/mat05.input

%doc README BUGS NEWS ChangeLog TODO AUTHORS



           













