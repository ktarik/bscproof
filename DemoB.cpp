/************************************************************/
/*@brief: HPME Demo B									    */ 
/*		  Matrix multiplication comparison					*/
/*		   A- CPU											*/
/*		   B- OpenCL										*/
/*		   Based on samples found in the OpenCL kit			*/
/*															*/		
/*@author: TKR												*/
/*															*/
/*															*/
/************************************************************/

//@TODO: 

int main(int argc, char *argv[]) 
{

   cl_context clGPUContext;
   cl_command_queue clCommandQue;
   cl_program clProgram;
   cl_kernel clKernel;
  
   size_t dataBytes;
   size_t kernelLength;
   cl_int errcode;
 
   cl_mem dev_A;
   cl_mem dev_B;
   cl_mem dev_C;
 

   // init OpenCL
   clGPUContext = clCreateContextFromType(0, 
                   CL_DEVICE_TYPE_GPU, 
                   NULL, NULL, &errcode);
   shrCheckError(errcode, CL_SUCCESS);
 
   errcode = clGetContextInfo(clGPUContext, 
              CL_CONTEXT_DEVICES, 0, NULL, 
              &dataBytes);
   cl_device_id *clDevices = (cl_device_id *)
              malloc(dataBytes);
   errcode |= clGetContextInfo(clGPUContext, 
              CL_CONTEXT_DEVICES, dataBytes, 
              clDevices, NULL);
   shrCheckError(errcode, CL_SUCCESS);
 
   clCommandQue = clCreateCommandQueue(clGPUContext, 
                  clDevices[0], 0, &errcode);
   shrCheckError(errcode, CL_SUCCESS);
  
   // setup device memory
   dev_C = clCreateBuffer(clGPUContext, 
          CL_MEM_READ_WRITE, 
          mem_size_A, NULL, &errcode);
   dev_A = clCreateBuffer(clGPUContext, 
          CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, 
          mem_size_A, h_A, &errcode);
   dev_B = clCreateBuffer(clGPUContext, 
          CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, 
          mem_size_B, h_B, &errcode);
 
 
   // load kernel definition
   char *clMatrixMul = oclLoadProgSource("matMul.cl","// test C\n",  &kernelLength);
   shrCheckError(clMatrixMul != NULL, shrTRUE);
 
   clProgram = clCreateProgramWithSource(clGPUContext, 
                1, (const char **)&clMatrixMul, 
                &kernelLength, &errcode);
   shrCheckError(errcode, CL_SUCCESS);
 
   errcode = clBuildProgram(clProgram, 0, 
              NULL, NULL, NULL, NULL);
   shrCheckError(errcode, CL_SUCCESS);
 
   clKernel = clCreateKernel(clProgram, 
               "matMul", &errcode);
   shrCheckError(errcode, CL_SUCCESS);
 
 
   /*
   size_t localworksiz[2]
   size_t globalworksize[2];
 
   int widthA = 16;
   int widthC = 16;
   errcode = clSetKernelArg(clKernel, 0, 
              sizeof(cl_mem), (void *)&dev_C);
   errcode |= clSetKernelArg(clKernel, 1, 
              sizeof(cl_mem), (void *)&dev_A);
   errcode |= clSetKernelArg(clKernel, 2, 
              sizeof(cl_mem), (void *)&dev_B);
   errcode |= clSetKernelArg(clKernel, 3, 
              sizeof(int), (void *)&wA);
   errcode |= clSetKernelArg(clKernel, 4, 
              sizeof(int), (void *)&wC);
   shrCheckError(errcode, CL_SUCCESS);
 
   localWorkSize[0] = 16;
   localWorkSize[1] = 16;
   globalWorkSize[0] = 1024;
   globalWorkSize[1] = 1024;
 
   errcode = clEnqueueNDRangeKernel(clCommandQue, 
              clKernel, 2, NULL, globalWorkSize, 
              localWorkSize, 0, NULL, NULL);
   shrCheckError(errcode, CL_SUCCESS);
 

   // read back results
   errcode = clEnqueueReadBuffer(clCommandQue, 
              dev_C, CL_TRUE, 0, mem_size_C, 
              h_C, 0, NULL, NULL);
   shrCheckError(errcode, CL_SUCCESS);
 
   
   free(h_A);
   free(h_B);
   free(h_C);
 
 */
   // bail out
   clReleaseMemObject(dev_A);
   clReleaseMemObject(dev_C);
   clReleaseMemObject(dev_B);
 
   free(clDevices);
   free(clMatrixMul);
   clReleaseContext(clGPUContext);
   clReleaseKernel(clKernel);
   clReleaseProgram(clProgram);
   clReleaseCommandQueue(clCommandQue);
   

}



