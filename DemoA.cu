
/************************************************************/
/*@brief: HPME Demo A									    */ 
/*		  Matrix multiplication comparison					*/
/*		   A- CPU											*/
/*		   B- CUDA API Naive implementation					*/
/*		   C- CUDA BLAS										*/
/*		   Based on samples found in the CUDA Toolkit		*/
/*															*/		
/*		   Loads two MM matrix files and computes			*/
/*		   result using each implementation sequentially	*/
/*@author: TKR												*/
/*															*/
/*															*/
/************************************************************/




#include <stdio.h>
#include <stdlib.h>


// define gettimeofday for WIN32 platform 
#ifndef _WIN32
#include <sys/time.h>
#include <unistd.h>
#else
#include <time.h>
#include "mytime.h"
#include <winsock.h> // struct timeval
#endif



#include <assert.h>

#include "cublas.h"

#include "mmio.h"

#include <math.h>

#define GETIDX(i, j, width) (j)*(width)+(i) // column-major 

typedef float matrix_type; /* matrix data type */

typedef struct {
	matrix_type *m;
	unsigned int dim_x;
	unsigned int dim_y;
} matrix;

matrix readMM(FILE* f);
matrix host_matrix(unsigned int dim_x, unsigned int dim_y);	// allocate a matrix on the HOST
matrix device_matrix(unsigned int dim_x, unsigned int dim_y); // allocate a matrix on the DEVICE
__global__ void mmult_na(matrix_type *c, matrix_type *a, matrix_type *b, unsigned, unsigned, unsigned);
// @TODO: find a decent algorithm that does this correctly 
__global__ void mmult_tl(matrix_type *c, matrix_type *a, matrix_type *b, unsigned, unsigned, unsigned);
__global__ void mmult_simple(float *a, float *b, float *ab, size_t width); // warm up !





void matrixMulA(matrix*, matrix*, matrix*);			// multiplication algo A
void matrixMulB( float* M, float* N, float* R, int Width); // multiplication algo B
void matricesAdd(matrix*, matrix*, matrix*); // addition
void printMatrix(matrix*); 
bool matrix_equal(matrix*, matrix*);
double seconds(struct timeval *t);
void checkCUDAError(const char *msg);		// helper func straight from the samples
float randomFloat(float, float);		// return a random float between the two limits

int main(int argc, char *argv[]) {
	int mtx_dim = 1000;
	int warm_mtx_dim = 50;
	bool no_cpu_compute = 0;
	bool no_gpu_compute = 0;
	bool do_warm_up = 1;
	int warm_up_rounds = 3;
	bool generate_matrices = false;
	bool print_enabled = 0;
	FILE* ifile1, *ifile2;
	char options;

	matrix a, b;


	printf("HPME Demo A\tAuthor: TKR\n");
	printf("A * B = C computation on CPU, CUDA, CUDA BLAS\n");
	printf("Usage:\tHPME dimension (run tests with two generated matrices of dimxdim)\n");
	printf("\tHPME inputf1 inputf2 (run tests using input files, output results to stdout)\n");
	printf("\tHPME inputf1 inputf2 outputf1 (save resulting matrix in separate file\n");
	printf("\toptional parameter '-p' will display matrices to stdout\n");
	printf("\t '-w' will disable warm up\n");
	printf("\t '-c' will disable CPU compute\n");
	printf("\t '-g' will disable GPU compute\n");
	printf("\t--------------------------------------------------------\n");
	


	// parse command-line
	// none
	if(argc == 1 )
	{
		printf("No arguments specified\n");
		exit(1);

	}
	// A - Dimension only
	if( argc >= 2 )
	{
		  int dimension = atoi(argv[1]);
		  if( dimension )
		  {
			printf("%dx%d matrices will be generated to run the tests\n", dimension, dimension);
			generate_matrices = true;
			mtx_dim = dimension;
		  }
		 // else
		 // {
	//		  printf("Invalid matrix dimension\n");
//			  exit(1);
//		  }
		
	}

	// B - two input files
	if( argc == 3 )
	{
		if( strstr( argv[1], ".mm") )
		{
			if( (ifile1 = fopen(argv[1], "r")) == NULL )
			{
				printf("Failed to open input file %s\n", argv[1]);
				exit(1);
			}
			
			if( (ifile2 = fopen(argv[2], "r")) == NULL )
			{

			printf("Failed to open input file %s\n", argv[2] );
			exit(1);
			}

			printf("Reading MM %s..", argv[1]);
			a	= readMM(ifile1);
			printf("OK\n");
			printf("Reading MM %s..", argv[2]);
			b	= readMM(ifile2);
			printf("OK\n");

			mtx_dim = b.dim_x;
			printf("Dimension = %d x %d\n", mtx_dim, mtx_dim);

		}
	}


		
	// C - two input files, one output file


	// check optional parameters
	int i = 0; 
	for( i = 0 ; i < argc ; ++i )
	{
		if( strcmp(argv[i], "-p") == 0 )
			print_enabled = 1;
		if( strcmp(argv[i], "-w") == 0 )
			do_warm_up = 0;
		if( strcmp(argv[i], "-c") == 0 )
			no_cpu_compute = 1;
		if( strcmp(argv[i], "-g") == 0 )
			no_gpu_compute = 1;
	}

	if( do_warm_up )
		printf("Warm-up enabled. %d rounds will be performed using %d x %d matrices\n", warm_up_rounds, warm_mtx_dim, warm_mtx_dim );
	
	// profiling
	struct timeval start, end; 
	struct timezone tz;

	//matrix da, db, dc; // device matrices, unused
	matrix  cpu_result; 
	matrix warmupA, warmupB, warmupC;
	matrix_type *ma, *mb, *mc;
	matrix_type *wma, *wmb, *wmc;
	matrix gpu_result, cublas_result;



	// generate input matrices 
	if( generate_matrices )
	{
		printf("Generating matrices ... ");
		gettimeofday(&start, &tz);	
		a = host_matrix(mtx_dim, mtx_dim);
		b = host_matrix(mtx_dim, mtx_dim);
		// fill input matrices with random data
		for (int i=0; i<a.dim_x; i++) {
			for (int j=0; j<a.dim_y; j++) {
				a.m[GETIDX(i, j, a.dim_x)] = randomFloat(0.f, 10.f);
				b.m[GETIDX(i, j, a.dim_x)] = randomFloat(0.f, 10.f);
			}
		}
		gettimeofday(&end, &tz);
		printf("OK [ %g ] \n", seconds(&end) - seconds(&start));
	}


	if ( do_warm_up )
	{
		warmupA = host_matrix(warm_mtx_dim, warm_mtx_dim );
		warmupB = host_matrix(warm_mtx_dim, warm_mtx_dim );
		warmupC = host_matrix(warm_mtx_dim, warm_mtx_dim );

	}

	// alloc result matrix on host memory
	cpu_result = host_matrix(mtx_dim, mtx_dim);
	gpu_result = host_matrix(mtx_dim, mtx_dim);
	cublas_result = host_matrix(mtx_dim, mtx_dim);
	//da = device_matrix(mtx_dim, mtx_dim);
	//db = device_matrix(mtx_dim, mtx_dim);
	
	
	if (print_enabled) {
		printf("Matrix A:\n");
		printMatrix(&a);
		printf("Matrix B:\n");
		printMatrix(&b);
	}

	
	/******************************/
	/* CPU matrix multiplication  */
	/******************************/
	if( do_warm_up && !no_cpu_compute )
	{
		printf("CPU Warm up, performing %d rounds..\n", warm_up_rounds );
		for( int i = 0 ; i < warm_up_rounds ; ++i )
		{
			printf("\tWarm up round %d..", i );
			matrixMulB(warmupA.m, warmupB.m, warmupC.m, warm_mtx_dim );
			printf("done\n");
		}


	}

	if (!no_cpu_compute) {
		gettimeofday(&start, &tz);	// profiling start
		
		//multMatrices(&cpu_result, &a, &b);
		matrixMulB(a.m, b.m, cpu_result.m, mtx_dim );
		
		gettimeofday(&end, &tz);
		printf("CPU time: %g\n", seconds(&end) - seconds(&start));
		
		if (print_enabled) {
			printf(" CPU Matrix result: \n");
			printMatrix(&cpu_result);
		}
		
		//assert(matrix_equal(&a, &c));
	}
	
	/******************/
	/* GPU CUDA mmult */
	/******************/

	//Warm GPU up to get accurate timings
	//cudaMemcpy(&da, &a, sizeof(matrix), cudaMemcpyHostToDevice);
	//cudaMemcpy(&db, &b, sizeof(matrix), cudaMemcpyHostToDevice);
	//cudaMemcpy(&dc, &c, sizeof(matrix), cudaMemcpyHostToDevice);
	//mmult_simple<<<num_blocks,block_size>>>(d_a, d_b, d_c, n);

	if (!no_gpu_compute) {

		size_t memsize = sizeof(matrix_type)*mtx_dim*mtx_dim;
		cudaMalloc((void **)&ma, memsize);
		cudaMalloc((void **)&mb, memsize);
		cudaMalloc((void **)&mc, memsize);
		cudaMemcpy(ma, a.m, memsize, cudaMemcpyHostToDevice);
		cudaMemcpy(mb, b.m, memsize, cudaMemcpyHostToDevice);



		int threads_per_dim = 16; //16*16=256 blockdim * blockdim = total threads per block
		int blocks_per_dim = mtx_dim / threads_per_dim + ((mtx_dim % threads_per_dim == 0) ? 0: 1);
		int wblocks_per_dim = warm_mtx_dim / threads_per_dim + ((warm_mtx_dim % threads_per_dim == 0 ) ? 0 : 1 );

		printf("thread dim: %d  block dim: %d\n", threads_per_dim, blocks_per_dim);
		
		dim3 blockdim(threads_per_dim, threads_per_dim);
		dim3 griddim(blocks_per_dim, blocks_per_dim);
		dim3 wgriddim(wblocks_per_dim, wblocks_per_dim);
		
		if( do_warm_up )
		{
			size_t wmemsize = sizeof(matrix_type) * warm_mtx_dim * warm_mtx_dim ;
			cudaMalloc((void**)&wma, wmemsize);
			cudaMalloc((void**)&wmb, wmemsize);
			cudaMalloc((void**)&wmc, wmemsize);
			cudaMemcpy( wma, warmupA.m, wmemsize, cudaMemcpyHostToDevice);
			cudaMemcpy( wmb, warmupB.m, wmemsize, cudaMemcpyHostToDevice);
			
			printf("GPU Warm up, performing %d rounds..\n", warm_up_rounds );
			for( int i = 0 ; i < warm_up_rounds ; ++i )
			{
				printf("\tWarm up round %d..", i );
				mmult_simple<<<wgriddim, blockdim>>>( wma, wmb, wmc, warmupB.dim_x);
				printf("done\n");
			}
			cudaFree(wma); cudaFree(wmb); cudaFree(wmc);
		}
	
		gettimeofday(&start, &tz);	

		mmult_na <<< griddim, blockdim >>> (mc, ma, mb, a.dim_x, a.dim_y, b.dim_y);
		//mmult_tl <<< dimGrid, dimBlock >>> (mc, ma, mb, a.dim_x, a.dim_y, b.dim_y);
		cudaThreadSynchronize();
		checkCUDAError("kernel invocation");
		
		gettimeofday(&end, &tz);
		printf("GPU-naive kernel time: %g\n", seconds(&end) - seconds(&start));
		
		cudaMemcpy(gpu_result.m, mc, memsize, cudaMemcpyDeviceToHost);
		checkCUDAError("device-to-host memcpy");
		cudaFree(ma); cudaFree(mb); cudaFree(mc);
		
		if (print_enabled) {
			printf(" GPU MMULT Matrix result: \n");
			printMatrix(&gpu_result);
		}
	}
	
	/******************/
	/* cuBLAS mmult   */
	/******************/
	cublasStatus status;
	matrix_type *cublas_a, *cublas_b, *cublas_c;

	status = cublasInit();
	
	status = cublasAlloc(mtx_dim*mtx_dim, sizeof(matrix_type), (void**)&cublas_a);
	status = cublasAlloc(mtx_dim*mtx_dim, sizeof(matrix_type), (void**)&cublas_b);
	status = cublasAlloc(mtx_dim*mtx_dim, sizeof(matrix_type), (void**)&cublas_c);
	
	status = cublasSetMatrix(mtx_dim, mtx_dim, sizeof(matrix_type),
							 a.m, mtx_dim, //host matrix, dimension
							 cublas_a, mtx_dim); //device matrix
	status = cublasSetMatrix(mtx_dim, mtx_dim, sizeof(matrix_type),
							 b.m, mtx_dim, //host matrix, dimension
							 cublas_b, mtx_dim); //device matrix

	cublasGetError(); 
	
	gettimeofday(&start, &tz);
	
	//matrix mult
	cublasSgemm('n', 'n',
				mtx_dim, mtx_dim, mtx_dim,
				1.0f, cublas_a, mtx_dim, cublas_b, mtx_dim,
				0.0f, cublas_c, mtx_dim);

	cudaThreadSynchronize();

	if (status != CUBLAS_STATUS_SUCCESS) {
		fprintf(stderr, "cublas-error doing matrix mult\n");
		char *msg;
		switch (status) {
			case CUBLAS_STATUS_NOT_INITIALIZED:
				msg = "not initialized"; break;
			case CUBLAS_STATUS_ALLOC_FAILED:
				msg = "alloc failed"; break;
			case CUBLAS_STATUS_INVALID_VALUE:
				msg = "invalid value"; break;
			case CUBLAS_STATUS_ARCH_MISMATCH:
				msg = "arch mismatch"; break;
			case CUBLAS_STATUS_MAPPING_ERROR:
				msg = "mapping error"; break;
			case CUBLAS_STATUS_EXECUTION_FAILED:
				msg = "gpu execution failed"; break;
			case CUBLAS_STATUS_INTERNAL_ERROR:
				msg = "internal error"; break;
			default:
				msg = "";
		}
		fprintf(stderr, "%s\n", msg);
		
		return EXIT_FAILURE;
	}
	
	gettimeofday(&end, &tz);
	
	status = cublasGetMatrix(mtx_dim, mtx_dim, sizeof(matrix_type),
							 cublas_c, mtx_dim,
							 cublas_result.m, mtx_dim);
	
	printf("GPU-cublas time: %g\n", seconds(&end) - seconds(&start));
	
	if (print_enabled) {
		printf(" GPU CUBLAS Matrix result: \n");
		printMatrix(&cublas_result);
	}
	

	// @TODO: some results are incorrect between CPU and GPU
	if (!no_cpu_compute) {
		//assert(matrix_equal(&cpu_result, &gpu_result));
		;//assert(matrix_equal(&cpu_result, &cublas_result));
	} else if (!no_gpu_compute) {
		assert(matrix_equal(&gpu_result, &cublas_result));
	}
	
	if( do_warm_up)
	{
		free(warmupA.m);
		free(warmupB.m);
		free(warmupC.m);
	}
	free(a.m);
	free(b.m);
	free(cpu_result.m);
	free(gpu_result.m);
	free(cublas_result.m);
}


// 
__global__ void mmult_simple(float *a, float *b, float *ab, size_t width)
{
	// row & column index of the element
	int row = blockIdx.y*blockDim.y + threadIdx.y;
	int col = blockIdx.x*blockDim.x + threadIdx.x;

	float result = 0;

	// dot product row of a and column of b
	for(int k = 0; k < width; ++k)
	{
		result += a[row*width+k] * b[k*width+col];
	}

	// write out this thread's result
	ab[row*width+col] = result;
}





__global__ void mmult_na(matrix_type *c, matrix_type *a, matrix_type *b,
							unsigned dim1, unsigned dim2, unsigned dim3) {
	int i = blockIdx.x*blockDim.x + threadIdx.x;
	int j = blockIdx.y*blockDim.y + threadIdx.y;
	int k = 0;

	// optimization: sum total in register up instead of copying back to host every time
	register matrix_type total = 0; 
	
	
	/* optimization: avoid useless multiplications */
	if (i >= dim1 || j >= dim3) return; 
	
	for (k = 0; k<dim2; k++) {
		total += a[GETIDX(i, k, dim2)] * b[GETIDX(k, j, dim3)];
	}
	c[GETIDX(i, j, dim3)] = total;
}

/*
// @TODO: this one is tricky
__global__ void mmult_tl(matrix_type *c, matrix_type *a, matrix_type *b,
							unsigned dim1, unsigned dim2, unsigned dim3) {
	//these are the coordinates of the result we're building up
	int result_i = blockIdx.x*blockDim.x + threadIdx.x; //
	int result_j = blockIdx.y*blockDim.y + threadIdx.y;
	
	unsigned int i, j;
	
	//copy data for this tile into shared mem
	__shared__ matrix_type sa[100];
	__shared__ matrix_type sb[100];

	matrix_type *sa = smem;
	matrix_type *sb = smem + sizeof(matrix_type)*blockDim.x*blockDim.y; //start b after a
	
	
	int sx = 0, sy = 0; //shared mem indices
 
	
}
*/

matrix readMM(FILE* f)
{
	MM_typecode matcode;

	matrix m;
	int M, N, nz;
    int i, *I, *J;
    double *val;
	int ret_code;


    if (mm_read_banner(f, &matcode) != 0)
    {
        printf("Could not process Matrix Market banner.\n");
        exit(1);
    }



	/* screen for MM type */
    if (mm_is_complex(matcode) && mm_is_matrix(matcode) && 
            mm_is_sparse(matcode) )
    {
        printf("Sorry, this application does not support ");
        printf("Market Market type: [%s]\n", mm_typecode_to_str(matcode));
        exit(1);
    }

    /* find out size of sparse matrix .... */
    if ((ret_code = mm_read_mtx_crd_size(f, &M, &N, &nz)) !=0)
	{
		printf("Couldn't find size of sparse matrix. \n");
        exit(1);
	}


	

    /* reseve memory for matrices */
    I = (int *) malloc(nz * sizeof(int));
    J = (int *) malloc(nz * sizeof(int));
    val = (double *) malloc(nz * sizeof(double));

    for (i = 0 ; i < nz ; i++)
    {
        fscanf(f, "%d %d %lg\n", &I[i], &J[i], &val[i]);
        I[i]--;  /* adjust from 1-based to 0-based */
        J[i]--;
		//printf("%d %d %lg\n", I[i], J[i], val[i]);
    }


	m.dim_x = M;
	m.dim_y = N;
	m.m = (matrix_type*)malloc(M * N * sizeof(matrix_type) );
	memset(m.m, 0.0f, M*N*sizeof(matrix_type) );


	
	int k = 0, l = 0;

	// convert coordinate format to internal matrix format
	/*
	for( i = 0; i < M ; ++i )
	{
		for( k = 0; k < N ; ++k )
		{

			m.m[I[i] + J[k] * N] = val[ I[i] * J[k] * N];
			printf("%d", I[i] * J[k] * N);
		}
	}*/

	for( int i = 0 ; i < nz ; ++i )
	{
		m.m[I[i] + J[i] * M] = val[i]; //val [I[i] + J[i] * M];
		//printf("%f ", m.m[I[i] + J[i]*M] );

	}


	free( I );
	free( J );
	free( val );


    if (f !=stdin) 
		fclose(f);

	return m;

}

void matricesAdd(matrix *result, matrix *a, matrix *b) {
	for (int y=0; y<a->dim_y*a->dim_x; y+=a->dim_x) {
		for (int x=0; x<a->dim_x; x++) {
			result->m[y+x] = a->m[y+x] + b->m[y+x];
		}
	}
}

void matrixMulA(matrix *result, matrix *m1, matrix *m2) {
	unsigned int dim1 = m1->dim_x,
				 dim2 = m1->dim_y,
				 dim3 = m2->dim_y;
	
	for (int i=0; i<dim1; i++) {
		for (int j=0; j<dim3; j++) {
			for (int k=0; k<dim2; k++) {
				result->m[GETIDX(i, j, dim3)] += m1->m[GETIDX(i, k, dim2)] * m2->m[GETIDX(k, j, dim3)];
				//result->m[i*dim3 + j] += m1->m[i*dim2 + k] * m2->m[k*dim3 + j];
			}
		}
	}
}


void matrixMulB( float* M, float* N, float* R, int width) {
	for (int i = 0; i < width; ++i)
		for (int j = 0; j < width; ++j) {
			float sum = 0;
			for (int k = 0; k < width; ++k) {
				float a = M[i * width + k];
				float b = N[k * width + j];
				sum += a * b;
			}
			R[i * width + j] = sum;
		}
}



bool matrix_equal(matrix *a, matrix *b) {
	if (a->dim_x != b->dim_x) return false;
	if (a->dim_y != b->dim_y) return false;
	
	double relativeDiff = 0;
	double total = 0;
	
	matrix_type* a_end = a->m + a->dim_x * a->dim_y;
	matrix_type* pa;
	matrix_type* pb;
	for (pa=a->m, pb=b->m; pa<a_end; pa++, pb++) {
		relativeDiff = abs((*pa - *pb)/((*pa + *pb)/2));
		total += relativeDiff;
		
		if (relativeDiff > .001) return false; //relative difference > .1%
		//if (*pa != *pb) return false;
	}
	
	printf("relative diff: %g\n", total/(a->dim_x*a->dim_y));
	
	return true;
}

matrix host_matrix(unsigned int dim_x, unsigned int dim_y) {
	matrix m;
	m.dim_x = dim_x;
	m.dim_y = dim_y;
	m.m = (matrix_type*)malloc(dim_x * dim_y * sizeof(matrix_type));
	return m;
}

matrix device_matrix(unsigned int dim_x, unsigned int dim_y) {
	matrix m;
	m.dim_x = dim_x;
	m.dim_y = dim_y;
	cudaMalloc((void **)&m.m, sizeof(matrix_type)*dim_x*dim_y);
	return m;
}

void printMatrix(matrix *a) {
	matrix_type *p;
	matrix_type *end = a->m + a->dim_y * a->dim_x;
	for (p = a->m; p < end; p++) {
		if ((p - a->m) % a->dim_x == 0) {
			printf("\n");
		}
		printf("%10.6g", *p);
	}
	printf("\n");
}

double seconds(struct timeval *t) {
	return t->tv_sec + ((double)t->tv_usec)/1.e6;
}

float randomFloat(float min, float max) {
	return rand()/(float(RAND_MAX)+1)*max + min;
}

void checkCUDAError(const char *msg) {
	cudaError_t err = cudaGetLastError();
	if (cudaSuccess != err) {
		fprintf(stderr, "Cuda error: %s: %s.\n", msg, cudaGetErrorString(err));
		exit(EXIT_FAILURE);
	}
}

