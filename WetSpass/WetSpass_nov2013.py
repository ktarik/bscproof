from numpy import *
from PCRaster import *
from decimal import *
import math
import os
import sys

getcontext().prec=28

########    Read Input Ascii maps and conversion to PCRaster format


def asc2map (FileNameIn, FileNameOut, CloneFile):     
     command='asc2map -a %s %s --clone %s -S -m -9999' %(FileNameIn, FileNameOut, CloneFile)
     os.system(command)
     
def map2asc (FileNameIn, FileNameOut, CloneFile):     
     command='map2asc %s %s --clone %s' %(FileNameIn, FileNameOut, CloneFile)
     os.system(command)
     
def Wetspass_function(elevation, soil, slope, landuse_w, landuse_s, rainfall_w, rainfall_s, PET_w, PET_s, Temp_w, Temp_s, Wind_w, Wind_s, GWdepth_w, GWdepth_s):
      
      ########    Read Input tables
      Impervious_Runoff_classes=lookupscalar("imp_classes.tbl",landuse_w)
      Veg_Runoff_classes=lookupscalar("veg_classes.tbl",landuse_w)
      interception_w=lookupscalar("intercepfactor_w.tbl",landuse_w)/100.0
      interception_s=lookupscalar("intercepfactor_s.tbl",landuse_w)/100.0
      Hortonian_w=lookupscalar("Hortonian_w.tbl",soil)
      Hortonian_s=lookupscalar("Hortonian_s.tbl",soil)
      VegArea_w=lookupscalar("VegArea_w.tbl",landuse_w)
      VegArea_s=lookupscalar("VegArea_s.tbl",landuse_w)
      BareArea_w=lookupscalar("BareArea_w.tbl",landuse_w)
      BareArea_s=lookupscalar("BareArea_s.tbl",landuse_w)
      ImpArea_w=lookupscalar("ImpArea_w.tbl",landuse_w)
      ImpArea_s=lookupscalar("ImpArea_s.tbl",landuse_w)
      OWArea_w=lookupscalar("OWArea_w.tbl",landuse_w)
      OWArea_s=lookupscalar("OWArea_s.tbl",landuse_w)
      zveg_w=lookupscalar("VegHeight_w.tbl",landuse_w)
      zveg_s=lookupscalar("VegHeight_s.tbl",landuse_w)
      LAI_w=lookupscalar("LAI_w.tbl",landuse_w)
      LAI_s=lookupscalar("LAI_s.tbl",landuse_w)
      minstomata_w=lookupscalar("minstomata_w.tbl",landuse_w)
      minstomata_s=lookupscalar("minstomata_s.tbl",landuse_w)
      A1=lookupscalar("A1.tbl",soil)                                                                              # Calibration Parameter
      FC=lookupscalar("field_capacity.tbl",soil)
      WP=lookupscalar("wilting_point.tbl",soil)
      Rootdepth_w=lookupscalar("rootdepth_w.tbl",landuse_w)
      Rootdepth_s=lookupscalar("rootdepth_s.tbl",landuse_w)
      Tension_ht=lookupscalar("tension_height.tbl",soil)
      Evapo_depth=lookupscalar("evapo_depth.tbl",soil)
      Residual_water_content=lookupscalar("res_water_content.tbl",soil)


      ########    Read Input tables
      Impervious_Runoff_classes=lookupscalar("imp_classes.tbl",landuse_w)
      Veg_Runoff_classes=lookupscalar("veg_classes.tbl",landuse_w)
      interception_w=lookupscalar("intercepfactor_w.tbl",landuse_w)/100.0
      interception_s=lookupscalar("intercepfactor_s.tbl",landuse_w)/100.0
      Hortonian_w=lookupscalar("Hortonian_w.tbl",soil)
      Hortonian_s=lookupscalar("Hortonian_s.tbl",soil)
      VegArea_w=lookupscalar("VegArea_w.tbl",landuse_w)
      VegArea_s=lookupscalar("VegArea_s.tbl",landuse_w)
      BareArea_w=lookupscalar("BareArea_w.tbl",landuse_w)
      BareArea_s=lookupscalar("BareArea_s.tbl",landuse_w)
      ImpArea_w=lookupscalar("ImpArea_w.tbl",landuse_w)
      ImpArea_s=lookupscalar("ImpArea_s.tbl",landuse_w)
      OWArea_w=lookupscalar("OWArea_w.tbl",landuse_w)
      OWArea_s=lookupscalar("OWArea_s.tbl",landuse_w)
      zveg_w=lookupscalar("VegHeight_w.tbl",landuse_w)
      zveg_s=lookupscalar("VegHeight_s.tbl",landuse_w)
      LAI_w=lookupscalar("LAI_w.tbl",landuse_w)
      LAI_s=lookupscalar("LAI_s.tbl",landuse_w)
      minstomata_w=lookupscalar("minstomata_w.tbl",landuse_w)
      minstomata_s=lookupscalar("minstomata_s.tbl",landuse_w)
      A1=lookupscalar("A1.tbl",soil)                                                                              # Calibration Parameter
      FC=lookupscalar("field_capacity.tbl",soil)
      WP=lookupscalar("wilting_point.tbl",soil)
      Rootdepth_w=lookupscalar("rootdepth_w.tbl",landuse_w)
      Rootdepth_s=lookupscalar("rootdepth_s.tbl",landuse_w)
      Tension_ht=lookupscalar("tension_height.tbl",soil)
      Evapo_depth=lookupscalar("evapo_depth.tbl",soil)
      Residual_water_content=lookupscalar("res_water_content.tbl",soil)

      ###################    CALCULATION OF WINTER WATER BALANCE
      ########    Process 1: Calculation of Winter Interception
      interception_w=interception_w*rainfall_w
      interception_w=ifthenelse(interception_w>PET_w,PET_w,interception_w)
      TotalInterception_w=interception_w*VegArea_w
      #report(TotalInterception_w,"TotalInterception_w.map")
      #map2asc('TotalInterception_w.map','TotalInterception_w.asc','clone.map')
      
      
      ########    Process 2: Calculation of Winter Runoff
      # Reclassify the landuse and slope grid
      slope_classes=ifthenelse(slope<=0.5,scalar(1),ifthenelse(slope<=5,scalar(2),ifthenelse(slope<=10,scalar(3),scalar(4))))       
      
      # Combine soil, vegtation runoff classes and slope classes
      Unique_Veg_Number=(100*soil)+(10*Veg_Runoff_classes)+(slope_classes)
      Veg_Runoff_Coefficient=lookupscalar("Veg_Runoff_Coefficient.tbl",Unique_Veg_Number)
      
      # Combine soil and slope classes 
      Unique_Bare_Number=(10*soil)+(slope_classes)
      Bare_Runoff_Coefficient=lookupscalar("Bare_Runoff_Coefficient.tbl",Unique_Bare_Number)
      
      # Combine impervious runoff classes and slope
      Unique_Impervious_Number=(10*Impervious_Runoff_classes)+(slope_classes)
      Impr_Runoff_Coefficient=ifthenelse(Unique_Impervious_Number>=11,lookupscalar("Imp_Runoff_Coefficient.tbl",Unique_Impervious_Number),scalar(0))       
      # As minimum value of unique number will be 1, for which no coefficient is defined in the table. Its a bug in the WetSpass ArcView.
       
      # Calculation of Potential Runoff
      potentialvegrunoff_w=Veg_Runoff_Coefficient*(rainfall_w-interception_w)
      #report(potentialvegrunoff_w,"potentialvegrunoff_w.map")
      #map2asc('potentialvegrunoff_w.map','potentialvegrunoff_w.asc','clone.map')
      
      potentialbarerunoff_w=Bare_Runoff_Coefficient*rainfall_w
      #report(potentialbarerunoff_w,"potentialbarerunoff_w.map")
      #map2asc('potentialbarerunoff_w.map','potentialbarerunoff_w.asc','clone.map')
      
      # Calculation of Runoff
      # Hortonian flow is infiltration excess flow while soil is still unsaturated, but rainfall intensity is higher. Should it be taken into account for vegetated surfaces or bare soil???
      # Runoff discharge area is set to 0.8, balance will contribute towards evapo-transpiration and recharge..
      
      vegrunoff_w=ifthenelse(GWdepth_w>scalar(0),(potentialvegrunoff_w*Hortonian_w),scalar(0.8)*(rainfall_w-interception_w))
      vegrunoff_w=ifthenelse(vegrunoff_w<0,scalar(0),vegrunoff_w)
      netvegrunoff_w=vegrunoff_w*VegArea_w
      #report(vegrunoff_w,"vegrunoff_w.map")
      #map2asc('vegrunoff_w.map','vegrunoff_w.asc','clone.map')
      
      barerunoff_w=ifthenelse(GWdepth_w>0,potentialbarerunoff_w*Hortonian_w,0.8*(rainfall_w-interception_w))
      # Interception from baresoil is taken into account in WetSpass ArcView? Should it be taken into account??
      barerunoff_w=ifthenelse(barerunoff_w<0,scalar(0),barerunoff_w)
      netbarerunoff_w=barerunoff_w*BareArea_w
      #report(barerunoff_w,"barerunoff_w.map")
      #map2asc('barerunoff_w.map','barerunoff_w.asc','clone.map')
      
      OWrunoff_w=ifthenelse(OWArea_w<=0,scalar(0),scalar(1.0)*(rainfall_w-PET_w))
      # Why there is need to multiply it grid (1). Further, is PET substraction prior to runoff is OK? 
      OWrunoff_w=ifthenelse(OWrunoff_w<0,scalar(0),OWrunoff_w)
      #report(OWrunoff_w,"OWrunoff_w.map")
      #map2asc('OWrunoff_w.map','OWrunoff_w.asc','clone.map')
      netOWrunoff_w=OWrunoff_w*OWArea_w
      
      Imperrunoff_w=rainfall_w*Impr_Runoff_Coefficient
      Imperrunoff_w=ifthenelse(Imperrunoff_w<0,scalar(0),Imperrunoff_w)
      netImperrunoff_w=Imperrunoff_w*ImpArea_w
      #report(Imperrunoff_w,"Imperrunoff_w.map")
      #map2asc('Imperrunoff_w.map','Imperrunoff_w.asc','clone.map')
      
      # Calculation of Summed Runoff
      TotalRunoff_w=netvegrunoff_w+netbarerunoff_w+netOWrunoff_w+netImperrunoff_w
      #report(TotalRunoff_w,"TotalRunoff_w.map")
      #map2asc('TotalRunoff_w.map','TotalRunoff_w.asc','clone.map')
      
      
      ########    Process 3: Calculation of Transpiration
      # zveg= vegetation height;    zo=roughness length;    zd=zero displacement length
      zd_w=scalar(0.67)*zveg_w
      RoughnessOM_w=scalar(0.123)*zveg_w
      # To avoid non-zero values and its use as denominator 
      RoughnessOM_w=ifthenelse(RoughnessOM_w>scalar(0),RoughnessOM_w,scalar(0.01))
      
      RoughnessOV_w=scalar(0.0123)*zveg_w
      RoughnessOV_w=ifthenelse(RoughnessOV_w>scalar(0),RoughnessOV_w,scalar(0.001))
      LAIoriginal_w=LAI_w
      LAI_w=ifthenelse(LAI_w>scalar(0),LAI_w,scalar(1))
      
      # Calculation of Penman Coefficient 
      gamma=0.66                     #Psychometric constant
      a=scalar(25083.0)
      b_w=Temp_w+scalar(237.3)
      c=scalar(2.71828)
      d=scalar(17.3)
      PenmannCoefficient_w=gamma/((a/sqr(b_w))*(pow(c,(d*Temp_w/b_w))))
      #report(PenmannCoefficient_w,'PenmannCoefficient_w.map')
      #map2asc('PenmannCoefficient_w.map','PenmannCoefficient_w.asc','clone.map')
      
      # Wind speed measurement level is set to 2.0 m
      windspeed_measurement_level=scalar(2.0)
      old_windspeed_ML=windspeed_measurement_level
      WSML_w=ifthenelse(windspeed_measurement_level<=zveg_w,(zveg_w+scalar(2.0)),windspeed_measurement_level)
      
      WSnominatorcor_w=Wind_w*ln((WSML_w-zd_w)/RoughnessOM_w)
      factor=(scalar(0.818)+ln(old_windspeed_ML+scalar(4.75)))
      Wind_w=ifthenelse(old_windspeed_ML<=zveg_w,WSnominatorcor_w/factor,Wind_w)
      WindMLRoughness_w=(ln((WSML_w-zd_w)/RoughnessOM_w))*(ln((WSML_w-zd_w)/RoughnessOV_w))
      
      Karman=0.4     #Defining Karman Constant
      Aerodynamicresist_w=WindMLRoughness_w*(scalar(1.0)/(sqr(Karman)*Wind_w))
      Canopyresist_w=minstomata_w/LAI_w
      Vegfactor_w=(scalar(1.0)+PenmannCoefficient_w)/(scalar(1.0)+((scalar(1.0)+(Canopyresist_w/Aerodynamicresist_w))*PenmannCoefficient_w))
      
      
      # Brooks and Corey bubbling capillary pressure
      Rootwater_w=((FC-WP)*(Rootdepth_w+Tension_ht)*scalar(1000))    # 1000 to convert from 'm' to 'mm'
      GWdepth_w=ifthenelse((GWdepth_w-Tension_ht)>=0,(GWdepth_w-Tension_ht),scalar(0))
      AWC_w=(rainfall_w+(scalar(12)*Rootwater_w))            # Why, its multiplied by 12????? 
      PETVeg_w=Vegfactor_w*PET_w
      A1PowerVeg_w=AWC_w/PETVeg_w
      # To limit the value of A1PowerVegetation             From where 30.0 came????
      A1PowerVeg_w=ifthenelse(A1PowerVeg_w>scalar(30.0),scalar(30.0),A1PowerVeg_w)
      #report(A1PowerVeg_w,'A1PowerVeg_w.map')
      #map2asc('A1PowerVeg_w.map','A1PowerVeg_w.asc','clone.map')
      
      
      #Actual Transpiration Calculation
      PotentialtoActualtranspirationfactor_w=(scalar(1.0)-(pow(A1,A1PowerVeg_w)))
      #report(PotentialtoActualtranspirationfactor_w,'PotentialtoActualtranspirationfactor_w.map')
      #map2asc('PotentialtoActualtranspirationfactor_w.map','PotentialtoActualtranspirationfactor_w.asc','clone.map')
      
      Actualtranspiration_w=PotentialtoActualtranspirationfactor_w*PETVeg_w
      Actualtranspiration_w=ifthenelse(Actualtranspiration_w>(rainfall_w-interception_w-vegrunoff_w),(rainfall_w-interception_w-vegrunoff_w),Actualtranspiration_w)    
      Actualtranspiration_w=ifthenelse(Actualtranspiration_w>(PET_w-interception_w),(PET_w-interception_w),Actualtranspiration_w)
      Actualtranspiration_w=ifthenelse(LAIoriginal_w==scalar(0),scalar(0),Actualtranspiration_w)
      GWTranspiration_w=ifthenelse(GWdepth_w<=Rootdepth_w,(PETVeg_w-Actualtranspiration_w),scalar(0))
      Transpiration_w=ifthenelse(GWdepth_w<=Rootdepth_w,PETVeg_w,Actualtranspiration_w)                 # Look into detail, that means, left over PET is going to be deducted from the GW Evaporation.
      #report(Transpiration_w,'Transpiration_w.map')
      #map2asc('Transpiration_w.map','Transpiration_w.asc','clone.map')
      
      
      ##    Calculation of Baresoil Evaporation
      
      # Brooks and Corey bubbling capillary pressure to evaporation depth. Should'nt the factor of 12 be 6?????????
      A1PowerBaresoil_w=(rainfall_w+(scalar(12.0)*(FC-Residual_water_content)*(Evapo_depth+Tension_ht)*1000))/PET_w
      # To limit the value of A1PowerVegetation to a maximum limit
      A1PowerBaresoil_w=ifthenelse(A1PowerBaresoil_w>scalar(30.0),scalar(30.0),A1PowerBaresoil_w)                   #What is 30.0?????
      # Is there any theoretical background of this factor from the literature?? Does it represent Potential to Actual or vice versa????
      PotentoActualEvapofactorbare_w=(scalar(1.0)-(pow(A1,A1PowerBaresoil_w)))
      ActualBaresoilEvapo_w=PotentoActualEvapofactorbare_w*PET_w
      ActualBaresoilEvapo_w=ifthenelse(ActualBaresoilEvapo_w>(rainfall_w-barerunoff_w),(rainfall_w-barerunoff_w),ActualBaresoilEvapo_w)
      # Why actual estimate is being compared with PET value, why not evaporation??
      ActualBaresoilEvapo_w=ifthenelse(ActualBaresoilEvapo_w>PET_w,PET_w,ActualBaresoilEvapo_w)
      # We assume that evapotranspiration deficit is being compensated from groundwater evaporation in case of higher GWDepth?? Is it OK???
      GWEvapo_w=ifthenelse(GWdepth_w<Evapo_depth,(PET_w-ActualBaresoilEvapo_w),scalar(0))
      
      #    Should'nt it be the sum of Actual Bare soil Evapotation and GW_Evaporation
      ActualBaresoilEvapo_w=ifthenelse(GWdepth_w<Evapo_depth,PET_w,ActualBaresoilEvapo_w)
      #report(ActualBaresoilEvapo_w,"ActualBaresoilEvapo_w.map")
      #map2asc('ActualBaresoilEvapo_w.map','ActualBaresoilEvapo_w.asc','clone.map')
      
      ##    Calculation of Impervious Evaporation
      ImperviousEvapo2Precip_Ratio=scalar(0.85)
      ImperviousEvapo_w=ImperviousEvapo2Precip_Ratio*(rainfall_w-Imperrunoff_w)
      ImperviousEvapo_w=ifthenelse(ImperviousEvapo_w>PET_w,PET_w,ImperviousEvapo_w)
      #report(ImperviousEvapo_w,"ImperviousEvapo_w.map")
      #map2asc('ImperviousEvapo_w.map','ImperviousEvapo_w.asc','clone.map')
      
      ##    Calculation of weighted summed evapo-transpiration
      Total_Actualtranspiration_w=Transpiration_w*VegArea_w
      #report(Total_Actualtranspiration_w,"Total_Actualtranspiration_w.map")
      #map2asc('Total_Actualtranspiration_w.map','Total_Actualtranspiration_w.asc','clone.map')
      Total_ActualBaresoilEvapo_w=ActualBaresoilEvapo_w*BareArea_w
      Total_OWEvaporation_w=PET_w*OWArea_w
      Total_ImperEvaporation_w=ImperviousEvapo_w*ImpArea_w
      Total_GWTranspiration_w=GWTranspiration_w*VegArea_w
      Total_GWEvaporation_w=GWEvapo_w*BareArea_w

      Total_Winter_Evapotranspiration=TotalInterception_w+Total_Actualtranspiration_w+Total_ActualBaresoilEvapo_w+Total_OWEvaporation_w+Total_ImperEvaporation_w+Total_GWTranspiration_w+Total_GWEvaporation_w
      #report(Total_Winter_Evapotranspiration,"Total_Winter_Evapotranspiration.map")
      #map2asc('Total_Winter_Evapotranspiration.map','Total_Winter_Evapotranspiration.asc','clone.map')


      ########    Process 3: Calculation of Recharge
      Recharge_w=rainfall_w-Total_Winter_Evapotranspiration-TotalRunoff_w           # What about interception???????????
      Recharge_w=ifthenelse(OWArea_w>0,ifthenelse(Recharge_w<0,scalar(0),Recharge_w), Recharge_w)
      Recharge_w=ifthenelse(Recharge_w<0,ifthenelse(GWdepth_w>Rootdepth_w,scalar(0),Recharge_w),Recharge_w)
      #report(Recharge_w,"Recharge_w.map")
      #map2asc('Recharge_w.map','Recharge_w.asc','clone.map')
      WB_error_w=ifthenelse(Recharge_w<0,ifthenelse(GWdepth_w>Rootdepth_w,Recharge_w,scalar(0)),scalar(0))
      #report(WB_error_w,"WB_error_W.map")
      #map2asc('WB_error_w.map','WB_error_w.asc','clone.map')
      
      ###################    WINTER TO SUMMER SOIL WATER STORAGE

      #changed_Jef
      #SoilwaterStorage_w=VegArea_w*(rainfall_w-interception_w-vegrunoff_w-Transpiration_w)
      #SoilwaterStorage_w=ifthenelse(SoilwaterStorage_w<0,scalar(0),SoilwaterStorage_w)
      SoilwaterStorage_w = Recharge_w

      ###################    CALCULATION OF SUMMER WATER BALANCE
      ########    Process 1: Calculation of Summer Interception
      interception_s=interception_s*rainfall_s
      interception_s=ifthenelse(interception_s>PET_s,PET_s,interception_s)
      TotalInterception_s=interception_s*VegArea_s
      #report(TotalInterception_s,"TotalInterception_s.map")
      #map2asc('TotalInterception_s.map','TotalInterception_s.asc','clone.map')


      ########    Process 2: Calculation of Summer Runoff
      # Using same reclassification of landuse, soil and slope grids as for Winter calculations

      # Calculation of Potential Runoff
      potentialvegrunoff_s=Veg_Runoff_Coefficient*(rainfall_s-interception_s)
      #report(potentialvegrunoff_s,"potentialvegrunoff_s.map")
      #map2asc('potentialvegrunoff_s.map','potentialvegrunoff_s.asc','clone.map')
      
      potentialbarerunoff_s=Bare_Runoff_Coefficient*rainfall_s
      #report(potentialbarerunoff_s,"potentialbarerunoff_s.map")
      #map2asc('potentialbarerunoff_s.map','potentialbarerunoff_s.asc','clone.map')
      
      # Calculation of Runoff
      # Hortonian flow is infiltration excess flow while soil is still unsaturated, but rainfall intensity is higher. Should it be taken into account for vegetated surfaces or bare soil???
      # Runoff discharge area is set to 0.8, balance will contribute towards evapo-transpiration and recharge..
      
      vegrunoff_s=ifthenelse(GWdepth_s>scalar(0),(potentialvegrunoff_s*Hortonian_s),scalar(0.8)*(rainfall_s-interception_s))
      vegrunoff_s=ifthenelse(vegrunoff_s<0,scalar(0),vegrunoff_s)
      netvegrunoff_s=vegrunoff_s*VegArea_s
      #report(vegrunoff_s,"vegrunoff_s.map")
      #map2asc('vegrunoff_s.map','vegrunoff_s.asc','clone.map')
      
      barerunoff_s=ifthenelse(GWdepth_s>0,potentialbarerunoff_s*Hortonian_s,scalar(0.8)*(rainfall_s-interception_s))
      # Interception from baresoil is taken into account in WetSpass ArcView? Should it be taken into account??
      barerunoff_s=ifthenelse(barerunoff_s<0,scalar(0),barerunoff_s)
      netbarerunoff_s=barerunoff_s*BareArea_s
      #report(barerunoff_s,"barerunoff_s.map")
      #map2asc('barerunoff_s.map','barerunoff_s.asc','clone.map')
      
      OWrunoff_s=ifthenelse(OWArea_s<=0,scalar(0),scalar(1.0)*(rainfall_s-PET_s))
      #Why there is need to multiply it grid (1). Further, is PET substraction prior to runoff is OK? 
      OWrunoff_s=ifthenelse(OWrunoff_s<0,scalar(0),OWrunoff_s)
      #report(OWrunoff_s,"OWrunoff_s.map")
      #map2asc('OWrunoff_s.map','OWrunoff_s.asc','clone.map')
      netOWrunoff_s=OWrunoff_s*OWArea_s
      
      Imperrunoff_s=rainfall_s*Impr_Runoff_Coefficient
      Imperrunoff_s=ifthenelse(Imperrunoff_s<0,scalar(0),Imperrunoff_s)
      netImperrunoff_s=Imperrunoff_s*ImpArea_s
      #report(Imperrunoff_s,"Imperrunoff_s.map")
      #map2asc('Imperrunoff_s.map','Imperrunoff_s.asc','clone.map')
      
      # Calculation of Summed Runoff
      TotalRunoff_s=netvegrunoff_s+netbarerunoff_s+netOWrunoff_s+netImperrunoff_s
      #report(TotalRunoff_s,"TotalRunoff_s.map")
      #map2asc('TotalRunoff_s.map','TotalRunoff_s.asc','clone.map')
      
      
      ########    Process 3: Calculation of Transpiration
      # zveg= vegetation height;    zo=roughness length;    zd=zero displacement length
      zd_s=scalar(0.67)*zveg_s
      RoughnessOM_s=scalar(0.123)*zveg_s
      # To avoid non-zero values and its use as denominator 
      RoughnessOM_s=ifthenelse(RoughnessOM_s>scalar(0),RoughnessOM_s,scalar(0.01))
      
      RoughnessOV_s=scalar(0.0123)*zveg_s
      RoughnessOV_s=ifthenelse(RoughnessOV_s>scalar(0),RoughnessOV_s,scalar(0.001))
      LAIoriginal_s=LAI_s
      LAI_s=ifthenelse(LAI_s>scalar(0),LAI_s,scalar(1))
      
      # Calculation of Penman Coefficient 
      gamma=0.66                     #Psychometric constant
      a=scalar(25083.0)
      b_s=Temp_s+scalar(237.3)
      c=scalar(2.71828)
      d=scalar(17.3)
      PenmannCoefficient_s=gamma/((a/sqr(b_s))*(pow(c,(d*Temp_s/b_s))))
      
      # Wind speed measurement level is set to 2.0 m
      WSML_s=ifthenelse(windspeed_measurement_level<=zveg_s,(zveg_s+scalar(2.0)),windspeed_measurement_level)
      
      WSnominatorcor_s=Wind_s*ln((WSML_s-zd_s)/RoughnessOM_s)
      factor=(scalar(0.818)+ln(old_windspeed_ML+scalar(4.75)))
      Wind_s=ifthenelse(old_windspeed_ML<=zveg_s,WSnominatorcor_s/factor,Wind_s)
      WindMLRoughness_s=(ln((WSML_s-zd_s)/RoughnessOM_s))*(ln((WSML_s-zd_s)/RoughnessOV_s))
      
      Karman=0.4     #Defining Karman Constant
      Aerodynamicresist_s=WindMLRoughness_s*(scalar(1.0)/(sqr(Karman)*Wind_s))
      Canopyresist_s=minstomata_s/LAI_s
      Vegfactor_s=(scalar(1.0)+PenmannCoefficient_s)/(scalar(1.0)+((scalar(1.0)+(Canopyresist_s/Aerodynamicresist_s))*PenmannCoefficient_s))
      
      # Brooks and Corey bubbling capillary pressure
      Rootwater_s=((FC-WP)*(Rootdepth_s+Tension_ht)*scalar(1000))    # 1000 to convert from 'm' to 'mm'
      GWdepth_s=ifthenelse((GWdepth_s-Tension_ht)>=0,(GWdepth_s-Tension_ht),scalar(0))
      AWC_s=(rainfall_s+(scalar(12)*Rootwater_s))            # Why, its multiplied by 12????? 
      PETVeg_s=Vegfactor_s*PET_s
      A1PowerVeg_s=AWC_s/PETVeg_s
      # To limit the value of A1PowerVegetation             From where 30.0 came????
      A1PowerVeg_s=ifthenelse(A1PowerVeg_s>scalar(30.0),scalar(30.0),A1PowerVeg_s)
      
      #Actual Transpiration Calculation
      PotentialtoActualtranspirationfactor_s=(scalar(1.0)-(pow(A1,A1PowerVeg_s)))
      SoilwaterStorage_w=ifthenelse(SoilwaterStorage_w>Rootwater_s,Rootwater_s,SoilwaterStorage_w)
      Actualtranspiration_s=PotentialtoActualtranspirationfactor_s*PETVeg_s
      
      Actualtranspiration_s=ifthenelse(Actualtranspiration_s>(rainfall_s-interception_s-vegrunoff_s+SoilwaterStorage_w),(rainfall_s-interception_s-vegrunoff_s+SoilwaterStorage_w),Actualtranspiration_s)    
      Actualtranspiration_s=ifthenelse(Actualtranspiration_s>(PET_s-interception_s),(PET_s-interception_s),Actualtranspiration_s)
      Actualtranspiration_s=ifthenelse(LAIoriginal_s<=scalar(0),scalar(0),Actualtranspiration_s)
      GWTranspiration_s=ifthenelse(GWdepth_s<=Rootdepth_s,(PETVeg_s-Actualtranspiration_s),scalar(0))
      Transpiration_s=ifthenelse(GWdepth_w<=Rootdepth_s,PETVeg_s,Actualtranspiration_s)
      #report(Transpiration_s,"Transpiration_s.map")
      #map2asc('Transpiration_s.map','Transpiration_s.asc','clone.map')
      
      
      ##    Calculation of Baresoil Evaporation
      
      # Brooks and Corey bubbling capillary pressure to evaporation depth
      A1PowerBaresoil_s=(rainfall_s+(scalar(12.0)*(FC-Residual_water_content)*(Evapo_depth+Tension_ht)*1000))/PET_s
      # To limit the value of A1PowerVegetation
      A1PowerBaresoil_s=ifthenelse(A1PowerBaresoil_s>scalar(30.0),scalar(30.0),A1PowerBaresoil_s)                   #What is 30.0?????
      PotentoActualEvapofactorbare_s=(scalar(1.0)-(pow(A1,A1PowerBaresoil_s)))
      ActualBaresoilEvapo_s=PotentoActualEvapofactorbare_s*PET_s
      ActualBaresoilEvapo_s=ifthenelse(ActualBaresoilEvapo_s>(rainfall_s-barerunoff_s),(rainfall_s-barerunoff_s),ActualBaresoilEvapo_s)
      ActualBaresoilEvapo_s=ifthenelse(ActualBaresoilEvapo_s>PET_s,PET_s,ActualBaresoilEvapo_s)
      GWEvapo_s=ifthenelse(GWdepth_s<Evapo_depth,(PET_s-ActualBaresoilEvapo_s),scalar(0))
      
      #    Should not it be the sum of Actual Bare soil Evapotation and GW_Evaporation
      ActualBaresoilEvapo_s=ifthenelse(GWdepth_s<Evapo_depth,(PET_s),ActualBaresoilEvapo_s)
      #report(ActualBaresoilEvapo_s,"ActualBaresoilEvapo_s.map")
      #map2asc('ActualBaresoilEvapo_s.map','ActualBaresoilEvapo_s.asc','clone.map')
      
      ##    Calculation of Impervious Evaporation
      ImperviousEvapo_s=ImperviousEvapo2Precip_Ratio*(rainfall_s-Imperrunoff_s)
      ImperviousEvapo_s=ifthenelse(ImperviousEvapo_s>PET_s,PET_s,ImperviousEvapo_s)
      #report(ImperviousEvapo_s,"ImperviousEvapo_s.map")
      #map2asc('ImperviousEvapo_s.map','ImperviousEvapo_s.asc','clone.map')
      
      ##    Calculation of weighted summed evapo-transpiration
      Total_Actualtranspiration_s=Transpiration_s*VegArea_s
      Total_ActualBaresoilEvapo_s=ActualBaresoilEvapo_s*BareArea_s
      Total_OWEvaporation_s=PET_s*OWArea_s
      Total_ImperEvaporation_s=ImperviousEvapo_s*ImpArea_s
      Total_GWTranspiration_s=GWTranspiration_s*VegArea_s
      Total_GWEvaporation_s=GWEvapo_s*BareArea_s
      
      Total_Summer_Evapotranspiration=TotalInterception_s+Total_Actualtranspiration_s+Total_ActualBaresoilEvapo_s+Total_OWEvaporation_s+Total_ImperEvaporation_s+Total_GWTranspiration_s+Total_GWEvaporation_s
      #report(Total_Summer_Evapotranspiration,"Total_Summer_Evapotranspiration.map")
      #map2asc('Total_Summer_Evapotranspiration.map','Total_Summer_Evapotranspiration.asc','clone.map')
      
      
      ########    Process 3: Calculation of Recharge
      Recharge_s=rainfall_s-Total_Summer_Evapotranspiration-TotalRunoff_s           # What about interception???????????
      Recharge_s=ifthenelse(OWArea_s>0,ifthenelse(Recharge_s<0,scalar(0),Recharge_s), Recharge_s)
      Recharge_s=ifthenelse(Recharge_s<(scalar(0)-SoilwaterStorage_w),ifthenelse(GWdepth_s>Rootdepth_s,(scalar(0)-SoilwaterStorage_w),Recharge_s),Recharge_s)
      #report(Recharge_s,"Recharge_s.map")
      #map2asc('Recharge_s.map','Recharge_s.asc','clone.map')
      
      
      WB_error_s=ifthenelse(Recharge_s<(scalar(0)-SoilwaterStorage_w), ifthenelse(GWdepth_s>Rootdepth_s,(Recharge_s+SoilwaterStorage_w),scalar(0)),scalar(0))
      #report(WB_error_s,"WB_error_s.map")
      #map2asc('WB_error_s.map','WB_error_s.asc','clone.map')
      
      
      
      ###################    SUMMATION OF RESULTS FOR THE WHOLE YEAR
      Yearly_Interception=TotalInterception_w+TotalInterception_s
      Yearly_Runoff=TotalRunoff_w+TotalRunoff_s
      Yearly_Evapotranspiration=Total_Winter_Evapotranspiration+Total_Summer_Evapotranspiration
      Yearly_Transpiration=Transpiration_w+Transpiration_s
      Yearly_Baresoil_Evaporation=ActualBaresoilEvapo_w+ActualBaresoilEvapo_s
      Yearly_Recharge=Recharge_w+Recharge_s
      Yearly_WaterBalance_Error=WB_error_w+WB_error_s
      
      report(Yearly_Interception,"Yearly_Interception.map")
      #map2asc('Yearly_Interception.map','Yearly_Interception.asc','clone.map')
      yearly_runoff_name='Yearly_Runoff.map'
      report(Yearly_Runoff,yearly_runoff_name)
      #map2asc('Yearly_Runoff.map','Yearly_Runoff.asc','clone.map')
      yearly_ET_name='Yearly_ET.map'
      report(Yearly_Evapotranspiration,yearly_ET_name)
      #map2asc('Yearly_Evapotranspiration.map','Yearly_Evapotranspiration.asc','clone.map')
      #report(Yearly_Transpiration,"Yearly_Transpiration.map")
      #map2asc('Yearly_Transpiration.map','Yearly_Transpiration.asc','clone.map')
      #report(Yearly_Baresoil_Evaporation,"Yearly_Baresoil_Evaporation.map")
      #map2asc('Yearly_Baresoil_Evaporation.map','Yearly_Baresoil_Evaporation.asc','clone.map')
      yearly_recharge_name='Yearly_Recharge.map'
      report(Yearly_Recharge,yearly_recharge_name)
      #map2asc('Yearly_Recharge.map','Yearly_Recharge.asc','clone.map')
      report(Yearly_WaterBalance_Error,"Yearly_WaterBalance_Error.map")
      #map2asc('Yearly_WaterBalance_Error.map','Yearly_WaterBalance_Error.asc','clone.map')


# Main
test = ["elevation", "soil", "slope", "landuse_w", "landuse_s", "ppt_w", "ppt_s", "PET_w", "PET_s", "Temp_w", "Temp_s", "Wind_w", "Wind_s", "GWdepth_w", "GWdepth_s"]

for element in test:
     asc2map(element+".asc", element+".map", "Clone.map")


elevation=readmap('elevation.map')
soil=readmap("soil.map")
slope=readmap('slope.map')
landuse_w=readmap("landuse_w.map")
landuse_s=readmap("landuse_s.map")
rainfall_w=readmap("ppt_w.map")
rainfall_s=readmap("ppt_s.map")
PET_w=readmap("PET_w.map")
PET_s=readmap("PET_s.map")
Temp_w=readmap("Temp_w")
Temp_s=readmap("Temp_s")
Wind_w=readmap("Wind_w.map")
Wind_s=readmap("Wind_s.map")
GWdepth_w=readmap("GWdepth_w.map")
GWdepth_s=readmap("GWdepth_s.map")

#run wetspass
Wetspass_function(elevation, soil, slope, landuse_w, landuse_s, rainfall_w, rainfall_s, PET_w, PET_s, Temp_w, Temp_s, Wind_w, Wind_s, GWdepth_w, GWdepth_s)


