__kernel void matMul(__global float* A,  __global float* B, __global float* C, int widthA, int widthB)
{
   int tx = get_global_id(0); 
   int ty = get_global_id(1);
 
   // thread computation result
   float result = 0;
   for (int k = 0; k < widthA; ++k)
   {
      float a = A[ty * widthA + k];
      float b = B[k * widthB + tx];
      result += a * b;
   }
 
   // each thread writes 1 result, write it to device mem
   C[ty * widthA + tx] = result;
}

